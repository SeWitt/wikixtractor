import java.util.Set;

/**
 * a custom container containing various sets of Page and Entity Objects extracted from nodes
 * @author andreas
 *
 */
public class NodeContainer {
	
	private static Set<Page> pages;
	private static Set<Person> people;
	private static Set<City> cities;
	private static Set<Monument> monuments;
	
	/**
	 * init container with given Sets
	 * @param pg a Set of Page Objects
	 * @param ppl a Set of Person Objects
	 * @param cs a Set of City Objects
	 * @param ms a Set of Monument Objects
	 */
	public NodeContainer(Set<Page> pg, Set<Person> ppl, Set<City> cs, Set<Monument> ms){
		pages = pg;
		people = ppl;
		cities = cs;
		monuments = ms;
	}

	public Set<Page> getPages() {
		return pages;
	}

	public void setPages(Set<Page> pages) {
		NodeContainer.pages = pages;
	}

	public Set<Person> getPeople() {
		return people;
	}

	public void setPeople(Set<Person> people) {
		NodeContainer.people = people;
	}

	public Set<City> getCities() {
		return cities;
	}

	public void setCities(Set<City> cities) {
		NodeContainer.cities = cities;
	}

	public Set<Monument> getMonuments() {
		return monuments;
	}

	public void setMonuments(Set<Monument> monuments) {
		NodeContainer.monuments = monuments;
	}

}
