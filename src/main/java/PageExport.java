import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
/**
 * Gets instances of pages, formates them into the required format.
 * If the writingprocess is successful, a xml-data is written, else
 * debug-logged with log4j.
 * @author Andreas Berger, Sebastian Witt, Dominik Albert, Ines Abou Saif
 *
 */

public class PageExport {
	/** Klasse für die statische Methode toXML */
	
	private static final Logger log4j = LogManager.getLogger("PageExport");
	private static int xmlformatcount;
	/** 
	 * builds xml and writes to specified file path
	 * @param FilePath dir of the file
	 * @param allnodes custom NodeContainer with various sets of Pages and Entities
	 * @param db neo4j GraphDatabaseService
	 * @throws IOException 
	 * builds xml according to the data in each Page
	 *
	 **/
	public static void toXML(String FilePath, NodeContainer allnodes, GraphDatabaseService db ) throws IOException{
		
		// split the container to the particular sets
		Set<Page> PageSet = allnodes.getPages();
		Set<Person> PersonSet = allnodes.getPeople();
		Set<City> CitySet = allnodes.getCities();
		Set<Monument> MonumentSet = allnodes.getMonuments();
		
		
		log4j.info("writing to file: " + FilePath + "...");
		XMLOutputFactory xof =  XMLOutputFactory.newInstance();
		XMLStreamWriter xtw = null;
		
		
		
		// remove all prefixed / or \ get clean path
		while(FilePath.startsWith("/") || FilePath.startsWith("\\")){
			FilePath = FilePath.substring(1);
		} 
		File dir = new File(FilePath);
		if(dir.exists() == false){
			log4j.debug("creating dir:\n" + dir + "\n"+ FilePath );
			Path pathToFile = Paths.get(FilePath);
			Files.createDirectories(pathToFile.getParent());
			Files.createFile(pathToFile);
		}
		
			
			
		
		
		try {
			xtw = xof.createXMLStreamWriter(new FileOutputStream(FilePath), "utf-8");
			// added inline formatting with custom static xmlformat(k) function:
			/*
			 * <pages>
			 *     <page attributes...>
			 *         <categories>
			 *         </categories>
			 *     </page>
			 * </pages>
			 * */
			xtw.writeStartDocument("utf-8","1.0");
			xtw.writeStartElement("wikixtractor");
			xtw.writeAttribute("tutorium", "4");
			xtw.writeAttribute("group", "G");
			xtw.writeStartElement("pages");
			int pagecount = 0;
			
			
			for(Page pageSetElement: PageSet){
				log4j.trace("trying to write: " + pageSetElement.getTitle());
				xtw.writeStartElement("page");
				xtw.writeAttribute("id", Long.toString(pageSetElement.getID()));
				xtw.writeAttribute("pageID", Integer.toString(pageSetElement.getPageID()));
				xtw.writeAttribute("namespaceID", Integer.toString(pageSetElement.getNamespaceID())); 
				xtw.writeAttribute("title", pageSetElement.getTitle());
				xtw.writeStartElement("categories");
				if (pageSetElement.getCategories() != null){
					for(String pageSetElementCategory: pageSetElement.getCategories()){
						xtw.writeStartElement("category");
					    xtw.writeAttribute("name", pageSetElementCategory);
					    xtw.writeEndElement();
				        }
				    }
				xtw.writeEndElement(); //End Categories Tag
				xtw.writeEndElement(); //End Page Tag
				pagecount += 1;
			    } 
			
			xtw.writeEndElement(); // Ends Pages Tag
			
			// Entity Tags Start here 
			xtw.writeStartElement("entities");
			
			// Entries for Person
			for(Person person: PersonSet){
				log4j.trace("trying to write: " + person.getName() + " to XML File");
				xtw.writeStartElement("person");
				xtw.writeAttribute("id", Long.toString(person.getID()));
				xtw.writeAttribute("sourceID", person.getSourceID());
				xtw.writeStartElement("properties");
				xtw.writeAttribute("name", person.getName());
				xtw.writeAttribute("birthname", person.getBirthname()); 
				xtw.writeAttribute("birthday", person.getBirthday());
				xtw.writeAttribute("dayofdeath", person.getDayOfDeath());
				xtw.writeAttribute("birthplace", person.getBirthplace());
				xtw.writeAttribute("lastlocation", person.getLastLocation());    
				xtw.writeEndElement();
				xtw.writeEndElement();
				pagecount += 1;
			} 
			
			//Entries for City
			for(City city: CitySet){
				xtw.writeStartElement("city");
				xtw.writeAttribute("id", Long.toString(city.getID()));
				xtw.writeAttribute("sourceID", city.getSourceID());
				xtw.writeStartElement("properties");
				xtw.writeAttribute("cityname", city.getCityName());
				xtw.writeAttribute("country", city.getCountry());
				xtw.writeAttribute("population", city.getPopulation());
				xtw.writeAttribute("foundingyear", city.getFoundingYear());
				xtw.writeEndElement(); //End Properties Tag
				xtw.writeEndElement(); //End City Tag
				pagecount += 1;
			}
			
			//Entries for Monument
			for(Monument monument: MonumentSet){
				xtw.writeStartElement("monument");
				xtw.writeAttribute("id", Long.toString(monument.getID()));
				xtw.writeAttribute("sourceID", monument.getSourceID());
				xtw.writeStartElement("properties");
				xtw.writeAttribute("monumentname", monument.getMonumentName());
				xtw.writeAttribute("location", monument.getLocation());
				xtw.writeAttribute("buildingYear", monument.getBuildingYear());
				if(monument.getMonumentPerson() != null){
					xtw.writeAttribute("monumentperson", monument.getMonumentPerson().getName());
				}else{
					xtw.writeAttribute("monumentperson", "-");
				}
				xtw.writeEndElement(); //End Properties Tag
				xtw.writeEndElement(); //End Monument Tag
				pagecount += 1;
			}
			log4j.info("wrote " + pagecount + " pages");
			
			xtw.writeEndElement(); // End Entities Tag
			xtw.writeEndElement(); // End Wikixtractor Tag
			xtw.writeEndDocument();
			xtw.flush();
			xtw.close();
			dir = null;
		} catch (XMLStreamException e) {
			if (xtw != null){
				try {
					xtw.close();
				} catch (XMLStreamException e1) {
					log4j.info("output file released");
				}
			}
			log4j.debug("Fehler beim erstellen der XML Datei, nur welcher?");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * create temporary xml and return for display
	 * @param allnodes Custom Container holding pages and entities
	 * @param db neo4j GraphDatabaseService
	 * @return String representation of a xml document
	 */
	public static String toWeb(NodeContainer allnodes, GraphDatabaseService db){
		String xmlString = "";
		
		Random rand = new Random();
		String alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
		int scope = alphabet.length();
		String random = "";
		for (int i=0; i<10;i++){
			random = random + alphabet.charAt(rand.nextInt(scope));
		}
		
		String filename = "TEMP" + random +".xml";
		String folder = Wikixtractor.getRootfolder() + "temp/";
		String path = folder.substring(1) + filename;
		
		try {
			toXML(path , allnodes, db);
			
			BufferedReader r;
	    	InputStreamReader streamreader;
	    	FileInputStream stream;
	    	File file;
			try {
	    		file = new File(path);
	    		stream = new FileInputStream(file);
	    		streamreader = new InputStreamReader(stream, Charset.forName("UTF-8"));
				r = new BufferedReader(streamreader);
				String line = null;
				log4j.debug("reading from file: " + path);
				while ((line = r.readLine()) != null){
					//make clean string
					xmlString = xmlString + line + "/n";
				}
				log4j.debug(xmlString);
				r.close();
				streamreader.close();
				stream.close();
				System.gc();
				while(file.exists()){
				    file.delete();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xmlString;
	}
}


