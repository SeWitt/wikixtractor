import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

/** Class for representing a "Monument" Labeled Nodes from the Database as an Monument Object.
 * Specifically for printing a Node to the Console.
 * 
 * @author Sebastian Witt
 *
 */
public class Monument extends Entity{
	private String sourceID;
	private Long ID;
	private String title;
	private String monumentName;
	private String location;
	private String buildingYear;
	private Person monumentPerson; //The Person the Monument is about. Needs to be the title of the Wiki Page of that Person
	private Node node;
	private static GraphDatabaseService db;
	private Set<Entity> monumentLinks = new HashSet<>();
	
	/**Constructor for Monuments Objects
	 * The Title of the Wiki Page is used to find all Informations in the Database using Cypher Querries.
	 * Every Node needs Properties with the exact same names, as the class attributes without the link Set plus the title Property.
	 * 
	 * @param title the Title of the node in DB
	 * @param DB neo4j GraphDatabaseService
	 */
	public Monument(String title, GraphDatabaseService DB) {
		db = DB;
		this.title = title;
		this.monumentName = Query.getMonumentName(title, DB);
		this.location = Query.getMonumentLocation(title, DB);
		this.buildingYear = Query.getMonumentYear(title, DB);
		this.monumentPerson = Query.getMonumentPerson(title, DB);
		this.monumentLinks = Query.getMonumentLinks(title, DB);
		this.sourceID = Query.getSourceID(title, DB, "Monument");
		Transaction tx = db.beginTx();
		this.ID = this.getNode().getId();
		tx.success();
		tx.close();
	}
	
	public String getSourceID() {
		return sourceID;
	}

	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	/** Method for Printing a Monument Object to the Console
	 * 
	 */
	@Override
	public void print(){
		System.out.println("Name des Monuments: " + this.monumentName);
		System.out.println("Steht in/bei: " + this.location);
		System.out.println("Gebaut im Jahre: " + this.buildingYear);
		System.out.println("Denkmal ist " + monumentPerson + " nachempfunden.");
		for(Entity a: monumentLinks){
			a.getName();

		}
	}
	
	/**Method for returning the Name of a Entity
	 * 
	 */
	@Override
	public String getName(){
		return this.monumentName;
	}

	public String getTitle() {
		return title;
	}

	
	public String getMonumentName() {
		return monumentName;
	}

	public String getLocation() {
		return location;
	}

	public String getBuildingYear() {
		return buildingYear;
	}

	public Person getMonumentPerson() {
		return monumentPerson;
	}

	public Set<Entity> getMonumentLinks() {
		return monumentLinks;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buildingYear == null) ? 0 : buildingYear.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((monumentName == null) ? 0 : monumentName.hashCode());
		result = prime * result + ((monumentPerson == null) ? 0 : monumentPerson.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Monument other = (Monument) obj;
		if (buildingYear == null) {
			if (other.buildingYear != null)
				return false;
		} else if (!buildingYear.equals(other.buildingYear))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (monumentName == null) {
			if (other.monumentName != null)
				return false;
		} else if (!monumentName.equals(other.monumentName))
			return false;
		if (monumentPerson == null) {
			if (other.monumentPerson != null)
				return false;
		} else if (!monumentPerson.equals(other.monumentPerson))
			return false;
		return true;
	}

	
	
	public Node getNode(){
		return Database.getNode( db, Label.label("Monument"), "0", this.title);
	}
	public void grabNode(){
		this.node = Database.getNode( db, Label.label("Monument"), "0", this.title);
	}
	public void dumpNode(){
		this.node = null;
	}
}
