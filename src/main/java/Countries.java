import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Countries {
	
	private static final Logger log4j = LogManager.getLogger("Countries");
	private static String rootfolder = Wikixtractor.getRootfolder();
	
	private Countries(){
		
	}
	/**
	 * parse the special html page from rootfolder and return a map of all countries with count numbers 0
	 * @return Map of countries with their respective initial matchcount 0
	 */
	public static Map<String, Integer> initMap(){
		String folder = rootfolder + "Alle L�nder der Welt von A-Z.htm";
		Map<String, Integer> countries = new HashMap<String, Integer>();
		// IO
    	BufferedReader r;
    	InputStreamReader streamreader;
    	FileInputStream stream;
    	File file;
		try {
    		file = new File(folder);
    		stream = new FileInputStream(file);
    		streamreader = new InputStreamReader(stream, Charset.forName("UTF-8"));
			r = new BufferedReader(streamreader);
			String line = null;
			String htmlpage = null;
			Document pagedoc = null;
			
			while ((line = r.readLine()) != null){
				//make clean string for parser
				htmlpage = htmlpage + line.trim();
			}
			pagedoc = Jsoup.parse(htmlpage);
			Elements tds = pagedoc.getElementsByTag("td");
			for (Element td : tds){
				Elements as = td.getElementsByTag("a");
				for (Element a : as){
					String country = a.childNode(0).toString();
					if ( country.contains("<") == false){
						countries.put(country, 0);
					}
				}
			}
			r.close();
			streamreader.close();
			stream.close();
		}catch(Exception e) {
			log4j.trace("no countryinit or corrupt file format - using preset data");
			countries.put("Serbien", 0);
			countries.put("Norwegen", 0);
			countries.put("Paraguay", 0);
			countries.put("Bahamas", 0);
			countries.put("Vatikanstadt", 0);
			countries.put("Heard und McDonaldinseln", 0);
			countries.put("Niederlande", 0);
			countries.put("Montserrat", 0);
			countries.put("Mali", 0);
			countries.put("Guadeloupe", 0);
			countries.put("Panama", 0);
			countries.put("Kenia", 0);
			countries.put("Laos", 0);
			countries.put("Katar", 0);
			countries.put("Seychellen", 0);
			countries.put("Kaimaninseln", 0);
			countries.put("Syrien", 0);
			countries.put("Belize", 0);
			countries.put("Bahrain", 0);
			countries.put("Guinea-Bissau", 0);
			countries.put("Juan de Nova", 0);
			countries.put("Tschechische Republik", 0);
			countries.put("Brasilien", 0);
			countries.put("Jamaika", 0);
			countries.put("Namibia", 0);
			countries.put("Saudi-Arabien", 0);
			countries.put("Frankreich (metropolitanes)", 0);
			countries.put("Eritrea", 0);
			countries.put("Puerto Rico", 0);
			countries.put("Rumänien", 0);
			countries.put("Trinidad und Tobago", 0);
			countries.put("Kongo, Demokratische Republik", 0);
			countries.put("Aruba", 0);
			countries.put("Neukaledonien", 0);
			countries.put("Mazedonien", 0);
			countries.put("Griechenland", 0);
			countries.put("Cookinseln", 0);
			countries.put("Deutschland", 0);
			countries.put("Madagaskar", 0);
			countries.put("Malawi", 0);
			countries.put("Andorra", 0);
			countries.put("Wallis und Futuna", 0);
			countries.put("Aserbaidschan", 0);
			countries.put("Liechtenstein", 0);
			countries.put("Kirgisistan", 0);
			countries.put("Kanada", 0);
			countries.put("Spanien", 0);
			countries.put("Bangladesch", 0);
			countries.put("Korea, Republik", 0);
			countries.put("Côte d'Ivoire", 0);
			countries.put("Tuvalu", 0);
			countries.put("Bolivien", 0);
			countries.put("Dänemark", 0);
			countries.put("Guernsey", 0);
			countries.put("Mikronesien", 0);
			countries.put("Politische Weltkarte", 0);
			countries.put("Mayotte", 0);
			countries.put("Israel", 0);
			countries.put("San Marino", 0);
			countries.put("Albanien", 0);
			countries.put("Kambodscha", 0);
			countries.put("Indien", 0);
			countries.put("Weihnachtsinsel", 0);
			countries.put("Myanmar", 0);
			countries.put("Kroatien", 0);
			countries.put("Gibraltar", 0);
			countries.put("St. Barthélemy", 0);
			countries.put("Malaysia", 0);
			countries.put("Südafrika", 0);
			countries.put("Oman", 0);
			countries.put("Swasiland", 0);
			countries.put("Serbien und Montenegro", 0);
			countries.put("Kolumbien", 0);
			countries.put("Bouvetinsel", 0);
			countries.put("Cabo Verde", 0);
			countries.put("Jersey", 0);
			countries.put("Mongolei", 0);
			countries.put("Marshallinseln", 0);
			countries.put("Ecuador", 0);
			countries.put("Zypern", 0);
			countries.put("Britische Jungferninseln", 0);
			countries.put("Slowakei", 0);
			countries.put("Österreich", 0);
			countries.put("Jordanien", 0);
			countries.put("Türkei", 0);
			countries.put("Estland", 0);
			countries.put("Vanuatu", 0);
			countries.put("Botsuana", 0);
			countries.put("Armenien", 0);
			countries.put("Honduras", 0);
			countries.put("Schweden", 0);
			countries.put("Nauru", 0);
			countries.put("Zentralafrikanische Republik", 0);
			countries.put("Haiti", 0);
			countries.put("Afghanistan", 0);
			countries.put("Burundi", 0);
			countries.put("Kasachstan", 0);
			countries.put("Litauen", 0);
			countries.put("Ruanda", 0);
			countries.put("São Tomé und Príncipe", 0);
			countries.put("St. Lucia", 0);
			countries.put("Südgeorgien und die Südlichen Sandwichinseln", 0);
			countries.put("China", 0);
			countries.put("Jemen", 0);
			countries.put("Martinique", 0);
			countries.put("Philippinen", 0);
			countries.put("Amerikanische Jungferninseln", 0);
			countries.put("Mexiko", 0);
			countries.put("Bhutan", 0);
			countries.put("Togo", 0);
			countries.put("Bosnien und Herzegowina", 0);
			countries.put("Niederländische Antillen", 0);
			countries.put("Spitzbergen", 0);
			countries.put("Grönland", 0);
			countries.put("Französisch-Polynesien", 0);
			countries.put("Westsahara", 0);
			countries.put("Färöer", 0);
			countries.put("Malediven", 0);
			countries.put("Montenegro", 0);
			countries.put("Bassas da India", 0);
			countries.put("Dominica", 0);
			countries.put("Benin", 0);
			countries.put("Tunesien", 0);
			countries.put("Angola", 0);
			countries.put("Sudan", 0);
			countries.put("Tromelin", 0);
			countries.put("Brunei Darussalam", 0);
			countries.put("Polen", 0);
			countries.put("Portugal", 0);
			countries.put("Westjordanland", 0);
			countries.put("Grenada", 0);
			countries.put("Irak", 0);
			countries.put("Iran", 0);
			countries.put("Kamerun", 0);
			countries.put("Guatemala", 0);
			countries.put("Guyana", 0);
			countries.put("Chile", 0);
			countries.put("Nepal", 0);
			countries.put("Saint-Martin", 0);
			countries.put("Belgien", 0);
			countries.put("Pitcairninseln", 0);
			countries.put("Ukraine", 0);
			countries.put("Dominikanische Republik", 0);
			countries.put("Amerikanisch-Samoa", 0);
			countries.put("Ghana", 0);
			countries.put("Anguilla", 0);
			countries.put("Island", 0);
			countries.put("Taiwan", 0);
			countries.put("Åland", 0);
			countries.put("Bermuda", 0);
			countries.put("Peru", 0);
			countries.put("Turkmenistan", 0);
			countries.put("Marokko", 0);
			countries.put("Großbritannien", 0);
			countries.put("Tansania", 0);
			countries.put("Tokelau", 0);
			countries.put("Guinea", 0);
			countries.put("Argentinien", 0);
			countries.put("Somalia", 0);
			countries.put("Vereinigte Staaten", 0);
			countries.put("Thailand", 0);
			countries.put("Kiribati", 0);
			countries.put("Costa Rica", 0);
			countries.put("Neuseeland", 0);
			countries.put("Mosambik", 0);
			countries.put("Vietnam", 0);
			countries.put("Gabun", 0);
			countries.put("St. Kitts und Nevis", 0);
			countries.put("Kuwait", 0);
			countries.put("Nigeria", 0);
			countries.put("Falklandinseln", 0);
			countries.put("Tadschikistan", 0);
			countries.put("Sri Lanka", 0);
			countries.put("Uruguay", 0);
			countries.put("Timor-Leste", 0);
			countries.put("Fidschi", 0);
			countries.put("Kuba", 0);
			countries.put("Kokosinseln (Keelinginseln)", 0);
			countries.put("Bulgarien", 0);
			countries.put("Samoa", 0);
			countries.put("Liberia", 0);
			countries.put("Venezuela", 0);
			countries.put("Lettland", 0);
			countries.put("Burkina Faso", 0);
			countries.put("Russische Föderation", 0);
			countries.put("St. Vincent und die Grenadinen", 0);
			countries.put("Palau", 0);
			countries.put("Französische Süd- und Antarktisgebiete", 0);
			countries.put("Italien", 0);
			countries.put("Insel Man", 0);
			countries.put("St. Helena, Ascension und Tristan da Cunha", 0);
			countries.put("Ägypten", 0);
			countries.put("Ungarn", 0);
			countries.put("Hongkong", 0);
			countries.put("Kongo", 0);
			countries.put("Südsudan", 0);
			countries.put("Niue", 0);
			countries.put("Frankreich", 0);
			countries.put("Äquatorialguinea", 0);
			countries.put("El Salvador", 0);
			countries.put("Monaco", 0);
			countries.put("Antigua und Barbuda", 0);
			countries.put("Clipperton", 0);
			countries.put("Papua-Neuguinea", 0);
			countries.put("Französisch-Guayana", 0);
			countries.put("Guam", 0);
			countries.put("Korea, Demokratische Volksrepublik", 0);
			countries.put("Lesotho", 0);
			countries.put("Tonga", 0);
			countries.put("Komoren", 0);
			countries.put("Réunion", 0);
			countries.put("Salomonen", 0);
			countries.put("Japan", 0);
			countries.put("Nördliche Marianen", 0);
			countries.put("Libanon", 0);
			countries.put("Belarus", 0);
			countries.put("Mauritius", 0);
			countries.put("Britisches Territorium im Indischen Ozean", 0);
			countries.put("Luxemburg", 0);
			countries.put("Äthiopien", 0);
			countries.put("Turks- und Caicosinseln", 0);
			countries.put("Senegal", 0);
			countries.put("Vereinigte Arabische Emirate", 0);
			countries.put("St. Pierre und Miquelon", 0);
			countries.put("Finnland", 0);
			countries.put("Simbabwe", 0);
			countries.put("Mauretanien", 0);
			countries.put("Tschad", 0);
			countries.put("Irland", 0);
			countries.put("Georgien", 0);
			countries.put("Singapur", 0);
			countries.put("Glorieuses", 0);
			countries.put("Macau", 0);
			countries.put("Antarktika", 0);
			countries.put("Sierra Leone", 0);
			countries.put("Kleinere Amerikanische Überseeinseln", 0);
			countries.put("Malta", 0);
			countries.put("Usbekistan", 0);
			countries.put("Pakistan", 0);
			countries.put("Gambia", 0);
			countries.put("Dschibuti", 0);
			countries.put("Europa", 0);
			countries.put("Indonesien", 0);
			countries.put("Algerien", 0);
			countries.put("Moldau", 0);
			countries.put("Slowenien", 0);
			countries.put("Niger", 0);
			countries.put("Gazastreifen", 0);
			countries.put("Norfolkinsel", 0);
			countries.put("Schweiz", 0);
			countries.put("Australien", 0);
			countries.put("Barbados", 0);
			countries.put("Nicaragua", 0);
			countries.put("Sambia", 0);
			countries.put("Uganda", 0);
			countries.put("Suriname", 0);
			countries.put("Libyen", 0);
		}
		return countries;
	}
	
	/**
	 * writes a text file with copy paste java code for a Map of countries
	 * @param countries the Map to save
	 */
	public static void writeInit(Map<String, Integer> countries){
		File write = new File(rootfolder+"initcountries.txt");
		try {
			write.createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(write.canWrite()){
			FileOutputStream streamout;
			try {
				streamout = new FileOutputStream(write);
				OutputStreamWriter streamwriter = new OutputStreamWriter(streamout);
				BufferedWriter w = new BufferedWriter(streamwriter);
			
				String output = "";
				Set<String> keys = countries.keySet();
				for (String key : keys){
					output= output + "countries.put(" +"\"" + key + "\"" + ", " +"0)"+ ";\n"; 
					
					
				}
				output = output + "}";
				log4j.info(output);
				w.write(output);
				w.close();
				streamwriter.close();
				streamout.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * counts hits and returns the best match
	 * @param countries the country map
	 * @return the Name of the Country with the most hits as String
	 */
	public static String getMax(Map<String, Integer> countries){
		String max = "Deutschland";
		
		Set<String> keys = countries.keySet();
		for (String key : keys){
			if (countries.get(key) > countries.get(max)){
				max = key;
			}
		}
		return max;
	}
}
