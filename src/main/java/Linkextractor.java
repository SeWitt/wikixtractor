//Data structures
import java.util.HashSet;
import java.util.Set;

//logger
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/**
 * a service class to extract links from Jsoup html Documents
 * cannot be instanced
 * @author Andreas Berger, Sebastian Witt, Dominik Albert, Ines Abou Saif
 *
 */

//A top-level Java class mimicking static class behavior
public final class Linkextractor {
	
	private static final Logger log4j = LogManager.getLogger("Linkextractor");
    
	private Linkextractor() { // private constructor
    }
	/**
	 * this method searches for a "catlink" html ID 
	 * which contains links to category pages
	 * and returns the title of those pages
	 * @param htmlpage a Jsoup html Document.
	 * @return returns Set of Strings containing the categories of page from html dump
	 */
    public static Set<String> getCategories(String htmlpage) {
    	
    	log4j.trace("extracting categories...");
    	Set<String> categories = new HashSet <>();
    	Element catlinks = null;
    	Document pagedoc = Jsoup.parse(htmlpage);    	
    	try{
    		catlinks = pagedoc.getElementById("catlinks");
    		Elements aTags = catlinks.getElementsByTag("a");
    		for(Element aTag :aTags){
    			String href = aTag.attr("href").toString();
    			if (href.contains("/wiki/Kategorie")){
    			    String title = aTag.attr("title").toString();
    			    log4j.trace(title);
    			    if (title != null){
    			        categories.add(title);
    			    }
    			}
    		}
    	}catch(NullPointerException e){
    		log4j.error("couldn't find any categories\n");
    		return categories;
    	}
    	log4j.trace("finished!");
        return categories;
    }
    
    
    /**
	 * this method searches for a Tags.
	 * if href attribute begins with "/wiki/" it is considered a valid link to an article
	 * @param htmlpage html String Document - will be parsed with jsoup.
	 * @return returns a Set of Strings containing titles of valid articles
	 */
    public static Set<String> getArticles(String htmlpage) {
    	
    	Set<String> articles = new HashSet <>();
    	Document pagedoc = Jsoup.parse(htmlpage);
    	try{
    		Elements aTags = pagedoc.getElementsByTag("a");
    		for(Element aTag :aTags){
    			String href = aTag.attr("href").toString();
    			if (href.contains("/wiki/")){
    			    String title = aTag.attr("title").toString();
    			    log4j.trace(title);
    			    if (title != null && title.equals("") == false){
    			        articles.add(title);
    			    }
    			}
    		}
    	}catch(NullPointerException e){
    		log4j.error("couldn't find any categories\n");
    		return articles;
    	}
    	log4j.trace("finished!");
        return articles;
    }
}
