import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

/**
 * a service class to extract links from Jsoup html Documents
 * cannot be instanced
 * @author Andreas Berger, Sebastian Witt, Dominik Albert, Ines Abou Saif
 *
 */

//A top-level Java class mimicking static class behavior
public final class Dataminer {
	
	private static final Logger log4j = LogManager.getLogger("Dataminer");
	private static GraphDatabaseService db;
	
	private Dataminer() { // private constructor
    }
	
	/**
	 * look up specific information to determine the entity type of this page
	 * @param htmlpage html page in String format
	 * @return the Entitytype as String
	 */
	public static String EntityBaseExtraction(String htmlpage){
		Document pagedoc = Jsoup.parse(htmlpage);
		String entitytype = "unknown";
		Element normdaten = pagedoc.getElementById("normdaten");
		if (normdaten != null){
			String norm = normdaten.attr("class").toString();
			switch(norm){ // TODO Denkmäler
				case "catlinks normdaten-typ-p":{
					entitytype = "Person";
					break;
				}
				case "catlinks normdaten-typ-g":{
					entitytype = "City";
					break;
				}
				default:{
					entitytype = "unknown";
					break;
				}
			}
		}else{
			Set<String> categories = Linkextractor.getCategories(htmlpage);
			for(String category : categories){
				//log4j.info(category);
				if(category.contains("Denkmal") || category.contains("denkmal") || category.contains("gedenkst"+"\u00E4"+"tte") || category.contains("Mahnmal")){
					entitytype = "Monument";
					break;
				}
			}
		}
		
		return entitytype;	
	}
	
/**	
	private static Entity getEntity(String title, String entitytype){
		Entity entity = null;
		log4j.info(entitytype);
		Element table = findTable(pagedoc, entitytype);
		log4j.info(table);
		switch(entitytype){
		case "Person":
			Map<String, String> personmap = extractNormData(table);
			log4j.info(personmap);
			entity = new Person(title, db);
			
			
			break;
		case "City":
			Map<String, String> citymap = extractCityTable(table);
			log4j.info(citymap);
		default:
			log4j.info("no table data");
		}
		return entity;
	}
	**/
	
	/**
	 * method to find specific table for specific entity type to extract normdata from
	 * @param pagedoc jsoup html Document 
	 * @param entitytype entitytype as string
	 * @return jsoup html element
	 */
	public static Element findTable(Document pagedoc, String entitytype){
		Elements tables = pagedoc.getElementsByTag("table");
		for (Element table : tables){
			switch(entitytype){
				case "Person":{ // should perform well all of the testet people seem to have this in common
					if (table.attr("id").toString().equals("Vorlage_Personendaten")){
						return table;
					}
					break;
				}
				case "City":{ // more complex as there are various table types for different cities _Infobox_ appears to be a common marker though
					if (table.attr("id").toString().contains("Infobox")){
						return table;
					}
					break;
				}
				default:{
					return null; // so far no monument showed any usefull table data - this one will be a pain :( 
				}
			}
		}
		return null;
	}
	
	/**
	 * extract relevant data for the entity type Person
	 * @param table the normtable extracted from a jsoup html Document
	 * @return a Map containing name,surname, birthname, birthday, birthplace, death, lastresidence
	 */
	public static Map<String, String> extractPerson(Element table){
		Map<String, String> map = new HashMap<String, String>();
		String name = "-", surname = "-", birthname = "-", birthday = "-", birthplace = "-", death = "-", lastresidence = "-";
		
		//log4j.info(table);
		if (table != null){
		Elements tds = table.getElementsByTag("td");
		String filter = "";
		for(Element td : tds){
			String tcontent = td.childNode(0).toString();
			//log4j.trace(tcontent);
			switch(filter){
			case "NAME":{
				String[] split = tcontent.split(",");
				if (split.length == 1){
					name = tcontent;
				}else{
					name = split[1];
					surname = split[0];
				}
				break;
			}
			case "ALTERNATIVNAMEN":{
				if (tcontent.contains("(Geburtsname)")){
					String[] split = tcontent.split(";");
					for(String altname : split){
						if (altname.contains("(Geburtsname)")){
							birthname = altname.replace("(Geburtsname)", "");
						}
					}
				}
			}
			case "GEBURTSDATUM":{
				birthday = tcontent;
				break;
			}
			case "GEBURTSORT":{
				Elements atags = td.getElementsByTag("a");
				for(Element atag : atags){
					birthplace = atag.childNode(0).toString();
				}
				break;
			}
			case "STERBEDATUM":{
				death = tcontent;
				break;
			}
			case "STERBEORT":{
				List<Node> children0 = td.childNodes();
				if(children0.isEmpty() == false){
					List<Node> children1 = td.childNode(0).childNodes();
				
					if(children1.isEmpty() == false){
						lastresidence = td.childNode(0).childNode(0).toString();
					}else{
						lastresidence = td.childNode(0).toString();
					}
				}
				break;
			}
			default:
				break;
			}
			filter = tcontent;
			//log4j.info(tcontent);
		}
		
		// TODO quickfix: set last residence to birthplace if person is still alive ( this is still largely inaccurate but oh well, time is money)
		if (lastresidence.equals("-") && death.equals("-")){
			lastresidence = birthplace;
		}
		}
		
		map.put("name", name);
		map.put("surname", surname);
		map.put("birthname", birthname);
		map.put("birthday", birthday);
		map.put("birthplace", birthplace);
		map.put("death", death);
		map.put("lastresidence", lastresidence);
		return map;
	}
	
	/**
	 * extract relevant data for the entity type City
	 * @param table the normtable extracted from a jsoup html Document
	 * @return Map containing population, foundingyear
	 */
	public static Map<String, String> extractCity(Element table){
		Map<String, String> map = new HashMap<String, String>();
        String population = "-", foundingyear = "-";
		
			// search table for population
        	if (table != null){
        	Elements tds = table.getElementsByTag("td");
			Boolean filter = false;
			for(Element td : tds){
				List<Node> childnode =td.childNodes();
				if(childnode.isEmpty() == false){
				String tcontent = td.childNode(0).toString();
				//log4j.trace(tcontent);
				if(filter == true){
					population = tcontent;
					filter = false;
				}
				if (tcontent.contains("\"Einwohner\"")){
					filter = true;
					//log4j.info("found population");
				}
				}
			
			}
        }
		
		map.put("population", population);
		map.put("foundingyear", foundingyear);
		return map;
	}
	
	/**
	 * extract relevant data for the entity type Monument
	 * @param DB the neo4j GraphDatabaseService
	 * @param htmlpage String representation of the htmlpage
	 * @return Map containing city, person, foundingyear
	 */
	public static Map<String, String> extractMonument(GraphDatabaseService DB, String htmlpage){
		Map<String, String> map = new HashMap<String, String>();
		Set<String> categories = Linkextractor.getCategories(htmlpage);
		Transaction tx = DB.beginTx();
		ResourceIterator<org.neo4j.graphdb.Node> cities = DB.findNodes(Label.label("City"));
		ResourceIterator<org.neo4j.graphdb.Node> people = DB.findNodes(Label.label("Person"));
		String city = "-", person = "-", foundingyear = "-";
		
		while(cities.hasNext()){
			org.neo4j.graphdb.Node citynode = cities.next();
			String title = citynode.getProperty("title").toString();
			for(String category : categories){
				if(category.contains(title)){
					city = title;
					break;
				}
			}
		}
		while(people.hasNext()){
			org.neo4j.graphdb.Node personnode = people.next();
			String title = personnode.getProperty("title").toString();
			for(String category : categories){
				if(category.contains(title)){
					person = title;
					break;
				}
			}
		}
		map.put("city", city);
		map.put("person", person);
		map.put("foundingyear", foundingyear);
		tx.success();
		tx.close();
		return map;
	}
	
	/**
	 * helper function that will count hits for certain countries in given html page
	 * @param countries map of countries and their hitcount
	 * @param htmlpage html source file as string representation
	 * @return a map of countries and their hits within given htmlpage
	 */
	public static Map<String, Integer> countCountries(Map<String, Integer> countries, String htmlpage){
		Set<String> keys = countries.keySet();
		for (String key : keys){
			String temp = htmlpage.replace(key, "");
			int count = ((htmlpage.length() - temp.length())/key.length());
			//if (count > 0){log4j.info(key + " " + count);}
			countries.put(key, count);
		}
		return countries;
	}
}
