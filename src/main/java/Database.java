import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterable;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.io.fs.FileUtils;


/** Class for all Methods on the Database
 * 
 */

/**
 * @author Andreas Berger, Sebastian Witt
 *
 */
public class Database {
	
	private static final Logger log4j = LogManager.getLogger("Database");
	private static GraphDatabaseService db;
	private static int linkcount;
	
	enum WikiRelations implements RelationshipType
	 {
	     has_category, has_friend, entity_link, is_entity_of
	 }
	
	private Database() { // private constructor
	}
	
	/*
	 * getters / setters
	 */
	

    /**
     * reset the Neo4j database or create new N4jdatabase in given path 
     * @param dbdirectory String path to the database directory 
     */
	public static void createDB(String dbdirectory){
		File dbDir = new File(dbdirectory);
		dbDir.mkdir();
		try{
		    if (dbDir.exists()){ 
		    	log4j.info("Found existing database at: " + dbdirectory + " - deleting");
		    	FileUtils.deleteRecursively (dbDir); // L�scht schon existierendes DB
		    	log4j.info("Success!");
		    }
		}catch(Exception e){
			log4j.error("Couldnt delete database at: " + dbDir);
			e.printStackTrace();
		}
		try{
		    log4j.info("creating new database at: " + dbdirectory);
			db = new GraphDatabaseFactory().newEmbeddedDatabase(dbDir); //Erstellt neue DB
			log4j.info("Try Indexing DB at" + dbdirectory);
			indexDBPageProperties();
		    log4j.info("Success!");
		}catch(Exception e){
			log4j.error("Couldnt create database at: " + dbDir);
			e.printStackTrace();
		}

	}
	/**
	 * fetch N4j DB Service from directory
	 * @param dbdirectory  String path to database
	 * @return GraphDatabaseService if a database exists at directory else null
	 */
	public static GraphDatabaseService openDB(String dbdirectory){
		File dbDir = new File(dbdirectory);
		if (dbDir.exists()){ // TODO actually check if there is a db not just the path
			GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabase(dbDir); //Erstellt neue DB
			return db;
		}
		else{
			log4j.error("No database at: " + dbdirectory);
			return null;
		}
	}	
	
	/**
	 * Indexes pageID, namespaceID, title for the Page Node
	 * @param dbdiretory
	 * @return void
	 */
	

	
	private static void indexDBPageProperties(){
		// Create Indices once and only when the db is newly created 
		if(true) { //TODO Check if Database is already indexed
		    try(Transaction tx	= db.beginTx()){
			    
		    	db.schema().indexFor(Label.label("Page")).on("pageID").create();
			    db.schema().indexFor(Label.label("Page")).on("namespaceID").create();
			    db.schema().indexFor(Label.label("Page")).on("title").create();
			    
			    db.schema().indexFor(Label.label("Person")).on("sourceID").create();
			    db.schema().indexFor(Label.label("Person")).on("namespaceID").create();
			    db.schema().indexFor(Label.label("Person")).on("title").create();
			    db.schema().indexFor(Label.label("Person")).on("surname").create();
			    db.schema().indexFor(Label.label("Person")).on("name").create();
			    db.schema().indexFor(Label.label("Person")).on("birthname").create();
			    db.schema().indexFor(Label.label("Person")).on("birthday").create();
			    db.schema().indexFor(Label.label("Person")).on("birthplace").create();
			    db.schema().indexFor(Label.label("Person")).on("dayOfDeath").create();
			    db.schema().indexFor(Label.label("Person")).on("lastLocation").create();
			    
			    db.schema().indexFor(Label.label("City")).on("sourceID").create();
			    db.schema().indexFor(Label.label("City")).on("title").create();
			    db.schema().indexFor(Label.label("City")).on("namespaceID").create();
			    db.schema().indexFor(Label.label("City")).on("cityName").create();
			    db.schema().indexFor(Label.label("City")).on("country").create();
			    db.schema().indexFor(Label.label("City")).on("foundingYear").create();
			    db.schema().indexFor(Label.label("City")).on("population").create();
			    
			    db.schema().indexFor(Label.label("Monument")).on("sourceID").create();
			    db.schema().indexFor(Label.label("Monument")).on("title").create();
			    db.schema().indexFor(Label.label("Monument")).on("namespaceID").create();
			    db.schema().indexFor(Label.label("Monument")).on("name").create();
			    db.schema().indexFor(Label.label("Monument")).on("location").create();
			    db.schema().indexFor(Label.label("Monument")).on("buildingYear").create();
			    db.schema().indexFor(Label.label("Monument")).on("monumentPerson").create();
			    
			    log4j.info("Indexing...");
			    tx.success();
			    }
		    catch(Exception e){
		    	log4j.error("Indexing Failed");
			    e.printStackTrace();			
		    }
	    }
	}
	/**
	 * look up a node in N4J Database and return it
	 * -Andreas
	 * @param DB Neo4J GraphdatabaseService 
	 * @param label the Label of the node
	 * @param namespaceID ...
	 * @param title ...
	 * @return N4j Node Element or null when nothing is found
	 */
	public static Node getNode(GraphDatabaseService DB, Label label, String namespaceID, String title){
    	log4j.debug("looking up nodes for: (" + namespaceID +", " + title + ")...");
    	db = DB;
    	Boolean check = false;
    	Transaction tx = null;
    	Node node = null;
	    try{
	    	tx = db.beginTx();
	    	ResourceIterator<Node> Nodes = db.findNodes(label, "title", title);
	    	if(Nodes != null){
    	        while(Nodes.hasNext()){
		    	    Node current = Nodes.next();
		    	    String curnamespace = (String)current.getProperty("namespaceID");
		    	    log4j.trace("current node: " + current + "namespaceID: " + namespaceID);
		    	    check = curnamespace.equals(namespaceID);
		    	    if (check){
		    		    node = current;
		    		    break;
		    	    }
		        }
    	    }
    	    tx.success();
	    }catch(Exception e){
	    	if (tx != null){tx.failure();}
	    }finally{
            if (tx != null){tx.close();}
	    }
        if (check){
    	    log4j.debug("...found node!");
 	        return node;
 	    }else{
 	    	log4j.debug("...couldn't find node!");
 	    	return null;
 	    }
    }
    /**
     * check if node already exists in Database
     * @param DB neo4j GraphDatabaseService
     * @param label ...
     * @param namespaceID ...
     * @param title ...
     * @return true if yes - false if no ^^
     */
    public static Boolean checkNode(GraphDatabaseService DB, Label label, String namespaceID, String title){
    	if (getNode(DB, label, namespaceID, title) != null){
    		return true;
    	}else{
    		return false;
    	}
    }
	
	/**
	 * look up htmldump of nodes and add links for categories to DB
	 * @param DB N4j GraphDatabaseService
	 */
	public static void addCategoryLinks(GraphDatabaseService DB){
		log4j.trace("adding category links");
		db = DB;
		linkcount = 0;
		Transaction tx = null;
		try{
			tx = db.beginTx();
			ResourceIterator<Node> nodes = db.findNodes(Label.label("Page"));
	 		Set<String> links = new HashSet <>();
	 		int i = 0;
	 		while(nodes.hasNext()){
	 			i = i + 1;
 		    	if (i % 500 == 0){
					log4j.info("processing node " + i);
 		    	}
	 			Node node = nodes.next();
	 			log4j.trace("extracting articles for " + node + "...");
	 			links = Linkextractor.getCategories((String)node.getProperty("html content"));
	 			addLinks(node, links, WikiRelations.has_category);
	 		}
	 		tx.success();
	 		log4j.info("added " + linkcount + " links");
	 		log4j.info("for " + i + " nodes");
	 		TaskScheduler.setCatlink_inserted(10);
		}catch(Exception e){
			tx.failure();
		}finally{
			tx.success();
		    if (tx != null){tx.close();} 
		}
 		
	}
	
	/**
	 * look up htmldump of nodes and add links between articles to DB
	 * @param DB N4j GraphDatabaseService
	 */
	public static void addArticleLinks(GraphDatabaseService DB){
		log4j.trace("adding category links");
		db = DB;
		linkcount = 0;
 		Set<String> links = new HashSet <>();
 		Transaction tx = null;
 		try{
 			tx =db.beginTx();
 			ResourceIterator<Node> nodes = db.findNodes(Label.label("Page"));
 			int i = 0;
 		    while(nodes.hasNext()){
 		    	i = i + 1;
 		    	if (i % 500 == 0){
					log4j.info("processing node " + i);
 		    	}
 			    Node node = nodes.next();
 			    if(node.getProperty("namespaceID").equals("0")){
 			        links = Linkextractor.getArticles((String)node.getProperty("html content"));
 			        addLinks(node, links, WikiRelations.has_friend);
 			    }
 		    }
 		    tx.success();
 		   log4j.info("added " + linkcount + " links");
 		   log4j.info("for " + i + " nodes");
 		  TaskScheduler.setArtlink_inserted(10);
	    }catch(Exception e){
	    	tx.failure();
	    }finally{
	    	
		    if (tx != null){tx.close();} 
	    }
	}
	
	
	
	/**
	 * add edges to the Graph
	 * @param db The Neo4j Graphdatabase Service to operate on
	 * @param node start node
	 * @param links Stringset of titles as end nodes 
	 */
	private static void addLinks(Node node, Set<String> links,  RelationshipType type){
		log4j.debug("adding " + type + " links "+ links + "\nfor node " + node);
		Transaction tx = null;
 		try{
 			tx = db.beginTx();
 			if(type == WikiRelations.has_friend ){
 				for(String link : links){
 					ResourceIterator<Node> others = db.findNodes(Label.label("Page"), "title", link);
 					while (others.hasNext()){
 						Node other = others.next();
 						if (other.getProperty("namespaceID").equals("0")){ // exclude articles this is for categories
 					        node.createRelationshipTo(other, type);
 					       linkcount += 1;
 					    }
 					}
 				}
 			}else{
 				for(String link : links){
 					ResourceIterator<Node> others = db.findNodes(Label.label("Page"), "title", link);
 					while (others.hasNext()){
 						Node other = others.next();
 						if (other.getProperty("namespaceID").equals("0") == false){ // exclude articles this is for categories
 					        node.createRelationshipTo(other, type);
 					       linkcount += 1;
 					    }
 					}
 				}
 			}
 			tx.success();
 			
 		}catch(Exception e){
 			tx.failure();
 		}finally{
 		    if (tx != null){tx.close();} 
 		}
		
	}

	/**
	 * creates a new page Node entry within the given N4j Database with given parameters
	 * @param db Neo4j GraphDatabaseService
	 * @param pageID ...
	 * @param namespaceID ...
	 * @param title ...
	 * @param htmlpage html formatted string
	 * @return true if successfull - false on error
	 */
	public static Boolean addnewPage(GraphDatabaseService db ,String pageID, String namespaceID, String title, String htmlpage){
		Transaction tx = null;
		try{
			if (db != null){
			    tx = db.beginTx();
			    
		    	log4j.debug("importing new Page: \n" + "pageID: " + pageID + " \n" + "namespaceID: " + namespaceID + " \n" + "title: " + title);
		        Node page = db.createNode(Label.label("Page")); // TODO add label based on category
		        page.setProperty("pageID", pageID);
		        page.setProperty("namespaceID", namespaceID);
	            page.setProperty("title", title);
                page.setProperty("html content", htmlpage);
			    Node newnode = db.findNode(Label.label("Page"), "pageID", pageID);
				log4j.debug("new page node: " + newnode);
			    
			    tx.success();
			    tx.close(); //aus Eclipse funktioniert es so mit der jar dann nicht
			    //if (tx != null){tx.close();} //Hier hat er mit der Jar über die Konsole einen Fehler ausgeworfen -Sebastian
			    return true;
			}else{
				log4j.error("the Database doesn't exist");
				return false;
			}
			
		}catch (Exception e){
			if (tx != null){tx.failure();}
			log4j.error("Transaction failed for new Page: \n" + "pageID: " + pageID + " \n" + "namespaceID: " + namespaceID + " \n" + "title: " + title);
			e.printStackTrace();
			if (tx != null){tx.close();}
			return false;
		}
	}
	
	
	// TODO the check whether an entity already exists in the DB should in fact be made withtin the addNewEntity() function types.
	// so for now they will remain private to prevent errors.
	
	/**
	 * extracts all entities( types, attributes) and adds them to the DB accordingly will link respective page with 'is_entity_of' relationship
	 * @param DB Neo4j GraphDatabaseService
	 */
	public static void addEntityLinks(GraphDatabaseService DB){
		
		log4j.trace("adding entity links");
		db = DB;
		linkcount = 0;
 		Transaction tx = null;
 		try{
 			tx =db.beginTx();
 			ResourceIterator<Node> nodes = db.findNodes(Label.label("Page"));
 			int i = 0;
 		    while(nodes.hasNext()){
 		    	i = i + 1;
 		    	if (i % 500 == 0){
					log4j.info("processing node " + i);
 		    	}
 			    Node node = nodes.next();
 			    log4j.trace("found node:\n" + node.toString() + node.getProperty("namespaceID").equals("0"));
 			    if(node.getProperty("namespaceID").equals("0")){
 			    	String htmlpage = node.getProperty("html content").toString();
 			    	//log4j.trace(htmlpage);
 			    	Document pagedoc = Jsoup.parse(htmlpage);
 			    	String entitytype = Dataminer.EntityBaseExtraction(htmlpage);
 			    	log4j.trace(entitytype);
 			    	Element table = Dataminer.findTable(pagedoc, entitytype);
 			    	//log4j.info(table);
 			    	Node entitynode = null;
 			    	if(entitytype != null){
 			    		switch(entitytype){
 			    		case "Person":{
 			    			Map<String, Integer> countries = Dataminer.countCountries(Countries.initMap(), htmlpage);
 			    			Map<String, String> personmap = Dataminer.extractPerson(table);
 			    			String title = node.getProperty("title").toString();
 			    			personmap.put("title", title);
 			    			personmap.put("lastresidence", Countries.getMax(countries));
 			    			personmap.put("sourceid", Long.toString(node.getId()));
 			    			log4j.debug(personmap);
 			    			Boolean check = Database.checkNode(db, Label.label("Person"), "0", title);
 			    			if (check != true){
 			    				entitynode = addNewPerson(db, personmap);
 			    			}
 			    			break;
 			    		}
 			    		case "City":{
 			    			Map<String, Integer> countries = Dataminer.countCountries(Countries.initMap(), htmlpage);
 			    			Map<String, String> citymap = Dataminer.extractCity(table);
 			    			String title = node.getProperty("title").toString();
 			    			citymap.put("title", title);
 			    			citymap.put("name", title);
 			    			citymap.put("country", Countries.getMax(countries));
 			    			citymap.put("sourceid", Long.toString(node.getId()));
 			    			log4j.debug(citymap);
 			    			Boolean check = Database.checkNode(db, Label.label("City"), "0", title);
 			    			if (check != true){
 			    				entitynode = addNewCity(db, citymap);
 			    			}
 			    			break;
 			    		}
 			    		case "Monument":{
 			    			Map<String, String> monmap = Dataminer.extractMonument(db, htmlpage);
 			    			String title = node.getProperty("title").toString();
 			    			monmap.put("title", title);
 			    			monmap.put("name", title);
 			    			monmap.put("sourceid", Long.toString(node.getId()));
 			    			log4j.debug(monmap);
 			    			Boolean check = Database.checkNode(db, Label.label("Monument"), "0", title);
 			    			if (check != true){
 			    				entitynode = addNewMonument(db, monmap);
 			    			}
 			    			break;
 			    		}
 			    		default:{
 			    			log4j.info("no table data");
 			    			break;
 			    		}
 			    		}
 			    		if (entitynode != null && node != null){
 			    			entitynode.createRelationshipTo(node, WikiRelations.is_entity_of);
 			    			linkcount += 1;
 			    		}
 			    	}
 			    }
 		    }
 		    tx.success();
 		   log4j.info("added " + linkcount + " Entity links");
 		   log4j.info("for " + i + " Page Nodes");
 		  TaskScheduler.setEntity_inserted(10);
	    }catch(Exception e){
	    	e.printStackTrace();
	    	tx.failure();
	    }finally{
	    	
		    if (tx != null){tx.close();} 
	    }
	}
	
	/**
	 * add new person type node to DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param personmap custom Map containing title, surname, name, birthname, birthday, birthplace, death, lastresidence
	 * @return newly created node for further processing
	 */
	private static Node addNewPerson(GraphDatabaseService DB, Map<String, String> personmap){
		String title = personmap.get("title"); 
		String surname = personmap.get("surname"); 
		String name = personmap.get("name");
		String birthname = personmap.get("birthname");
		String birthday = personmap.get("birthday");
		String birthplace = personmap.get("birthplace");
		String death = personmap.get("death");
		String lastresidence = personmap.get("lastresidence");
		String sourceID = personmap.get("sourceid");
		
		Transaction tx = null;
		try{
			if (DB != null){
			    tx = DB.beginTx();
			    //TODO check if node already exists
			    
		    	log4j.debug("importing new Person: \n" + "name: " + name + " \n" + "lastname: " + surname + " \n" + "title: " + title);
		        Node person = DB.createNode(Label.label("Person"));
		        person.setProperty("sourceID", sourceID);
		        person.setProperty("title", title);
		        person.setProperty("namespaceID", "0");
		        person.setProperty("surname", surname);
		        person.setProperty("name", name);
		        person.setProperty("birthname", birthname);
		        person.setProperty("birthday", birthday);
		        person.setProperty("birthplace", birthplace);
		        person.setProperty("dayOfDeath", death);
		        person.setProperty("lastLocation", lastresidence);
			    Node newnode = DB.findNode(Label.label("Person"), "title", title);
				log4j.debug("new person node: " + newnode);
			    
			    tx.success();
			    tx.close();
			    return person;
			}else{
				log4j.error("the Database doesn't exist");
				return null;
			}
			
		}catch (Exception e){
			if (tx != null){tx.failure();}
			log4j.error("Transaction failed for new Person: \n" + "name: " + name + " \n" + "lastname: " + surname + " \n" + "title: " + title);
			e.printStackTrace();
			if (tx != null){tx.close();}
			return null;
		}
	}
	
	/**
	 * add new city type Node into DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param map custom Map containing title, name, country, foundingyear, population
	 * @return newly created node for further processing
	 */
	private static Node addNewCity(GraphDatabaseService DB, Map<String, String> map){
		String title = map.get("title"); 
		String name = map.get("name"); 
		String country = map.get("country");
		String foundingyear = map.get("foundingyear");
		String population = map.get("population");
		String sourceID = map.get("sourceid");
		
		Transaction tx = null;
		try{
			if (DB != null){
			    tx = DB.beginTx();
			    //TODO check if node already exists
			    
		    	log4j.debug("importing new Person: \n" + "name: " + name + " \n" + "country: " + country + " \n" + "population: " + population);
		        Node node = DB.createNode(Label.label("City"));
		        node.setProperty("sourceID", sourceID);
		        node.setProperty("title", title);
		        node.setProperty("namespaceID", "0");
		        node.setProperty("cityName", name);
		        node.setProperty("country", country);
		        node.setProperty("foundingYear", foundingyear);
		        node.setProperty("population", population);
			    Node newnode = DB.findNode(Label.label("City"), "title", title);
				log4j.debug("new city node: " + newnode);
			    
			    tx.success();
			    tx.close();
			    return node;
			}else{
				log4j.error("the Database doesn't exist");
				return null;
			}
			
		}catch (Exception e){
			if (tx != null){tx.failure();}
			log4j.error("Transaction failed for new City: \n" + "name: " + name + " \n" + "country: " + country + " \n" + "population: " + population);
			e.printStackTrace();
			if (tx != null){tx.close();}
			return null;
		}
	}
	
	/**
	 * adds a new Monument type Node into the DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param map custom Map containing title, name, city, foundingyear, person
	 * @return newly created node for further processing
	 */
	private static Node addNewMonument(GraphDatabaseService DB, Map<String, String> map){
		String title = map.get("title"); 
		String name = map.get("name"); 
		String city = map.get("city");
		String foundingyear = map.get("foundingyear");
		String person = map.get("person");
		String sourceID = map.get("sourceid");
		
		Transaction tx = null;
		try{
			if (DB != null){
			    tx = DB.beginTx();
			    //TODO check if node already exists
			    
		    	log4j.debug("importing new Person: \n" + "name: " + name + " \n" + "city: " + city + " \n" + "person: " + person);
		        Node node = DB.createNode(Label.label("Monument"));
		        node.setProperty("sourceID", sourceID);
		        node.setProperty("title", title);
		        node.setProperty("namespaceID", "0");
		        node.setProperty("name", name);
		        node.setProperty("location", city);
		        node.setProperty("buildingYear", foundingyear);
		        node.setProperty("monumentPerson", person);
			    Node newnode = DB.findNode(Label.label("Monument"), "title", title);
				log4j.debug("new monument node: " + newnode);
			    
			    tx.success();
			    tx.close();
			    return node;
			}else{
				log4j.error("the Database doesn't exist");
				return null;
			}
			
		}catch (Exception e){
			if (tx != null){tx.failure();}
			log4j.error("Transaction failed for new City: \n" + "name: " + name + " \n" + "city: " + city + " \n" + "population: " + person);
			e.printStackTrace();
			if (tx != null){tx.close();}
			return null;
		}
	}
	
	/**
	 * packs all nodes in DB together with their appropriate representation 
	 * @param DB Neo4j GraphDatabaseService
	 * @return custom NodeContainer Object containing class representations of all DB entries
	 */
	public static NodeContainer allNodes(GraphDatabaseService DB){
		Set<Page> allpages= new HashSet<>();
		Set<Person> allpeople= new HashSet<>();
		Set<City> allcities= new HashSet<>();
		Set<Monument> allmonuments= new HashSet<>();
		Transaction tx = null;
		try {
			tx = DB.beginTx();
			
			ResourceIterable<Node> allpagenodes = DB.getAllNodes();
			for(Node node : allpagenodes){
				if (node.hasLabel(Label.label("Page"))){
					allpages.add(Pagefactory.makePage(DB, node));
				}
				if (node.hasLabel(Label.label("Person"))){
					allpeople.add(new Person(node.getProperty("title").toString() , DB));
				}
				if (node.hasLabel(Label.label("City"))){
					allcities.add(new City(node.getProperty("title").toString() , DB));
				}
				if (node.hasLabel(Label.label("Monument"))){
					allmonuments.add(new Monument(node.getProperty("title").toString() , DB));
				}
			}
			
			tx.success();
			tx.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (tx != null){
				tx.failure();
				tx.close();
			}
			e.printStackTrace();
		}
		return new NodeContainer(allpages, allpeople, allcities, allmonuments);
	}
	
	
	/**
	 * search for a page by title in DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param title the pagetitle
	 * @return custom Nodecontainer Object
	 */
	public static NodeContainer getPageContainerFromTitle(GraphDatabaseService DB, String title){
		Set<Page> allpages= new HashSet<>();
		Set<Person> allpeople= new HashSet<>();
		Set<City> allcities= new HashSet<>();
		Set<Monument> allmonuments= new HashSet<>();
		Transaction tx = null;
		try {
			tx = DB.beginTx();
			ResourceIterator<Node> nodes = DB.findNodes(Label.label("Page"), "title", title);
			while(nodes.hasNext()){
				Node node = nodes.next();
				allpages.add(Pagefactory.makePage(DB, node));
			}
			tx.success();
			tx.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (tx != null){
				tx.failure();
				tx.close();
			}
			e.printStackTrace();
		}
		return new NodeContainer(allpages, allpeople, allcities, allmonuments);
	}
	
	/**
	 * searchrequest for a persons name in DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param name the persons name
	 * @return custom NodeContainer Object
	 */
	public static NodeContainer getPersonContainerFromName(GraphDatabaseService DB, String name){
		Set<Page> allpages= new HashSet<>();
		Set<Person> allpeople= new HashSet<>();
		Set<City> allcities= new HashSet<>();
		Set<Monument> allmonuments= new HashSet<>();
		Transaction tx = null;
		try {
			tx = DB.beginTx();
			ResourceIterator<Node> nodes = DB.findNodes(Label.label("Person"), "name", name);
			while(nodes.hasNext()){
				Node node = nodes.next();
				allpeople.add(new Person(node.getProperty("title").toString() , DB));
			}
			tx.success();
			tx.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (tx != null){
				tx.failure();
				tx.close();
			}
			e.printStackTrace();
		}
		return new NodeContainer(allpages, allpeople, allcities, allmonuments);
	}
	
	/**
	 * Search request for a persons surname in DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param surname the persons surname
	 * @return custom NodeContainer Object
	 */
	public static NodeContainer getPersonContainerFromSurname(GraphDatabaseService DB, String surname){
		Set<Page> allpages= new HashSet<>();
		Set<Person> allpeople= new HashSet<>();
		Set<City> allcities= new HashSet<>();
		Set<Monument> allmonuments= new HashSet<>();
		Transaction tx = null;
		try {
			tx = DB.beginTx();
			ResourceIterator<Node> nodes = DB.findNodes(Label.label("Person"), "surname", surname);
			while(nodes.hasNext()){
				Node node = nodes.next();
				allpeople.add(new Person(node.getProperty("title").toString() , DB));
			}
			tx.success();
			tx.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (tx != null){
				tx.failure();
				tx.close();
			}
			e.printStackTrace();
		}
		return new NodeContainer(allpages, allpeople, allcities, allmonuments);
	}
	
	/**
	 * search request for city name in DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param name name of the city
	 * @return custom NodeContainer Object
	 */
	public static NodeContainer getCityContainerFromName(GraphDatabaseService DB, String name){
		Set<Page> allpages= new HashSet<>();
		Set<Person> allpeople= new HashSet<>();
		Set<City> allcities= new HashSet<>();
		Set<Monument> allmonuments= new HashSet<>();
		Transaction tx = null;
		try {
			tx = DB.beginTx();
			ResourceIterator<Node> nodes = DB.findNodes(Label.label("City"), "cityName", name);
			while(nodes.hasNext()){
				Node node = nodes.next();
				allcities.add(new City(node.getProperty("title").toString() , DB));
			}
			tx.success();
			tx.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (tx != null){
				tx.failure();
				tx.close();
			}
			e.printStackTrace();
		}
		return new NodeContainer(allpages, allpeople, allcities, allmonuments);
	}
	
	/**
	 * Searchrequest for Monuments name in DB
	 * @param DB Neo4j GraphDatabaseService
	 * @param name name of the Monument
	 * @return custom NodeContainer Object
	 */
	public static NodeContainer getMonumentContainerFromName(GraphDatabaseService DB, String name){
		Set<Page> allpages= new HashSet<>();
		Set<Person> allpeople= new HashSet<>();
		Set<City> allcities= new HashSet<>();
		Set<Monument> allmonuments= new HashSet<>();
		Transaction tx = null;
		try {
			tx = DB.beginTx();
			ResourceIterator<Node> nodes = DB.findNodes(Label.label("Monument"), "name", name);
			while(nodes.hasNext()){
				Node node = nodes.next();
				allmonuments.add(new Monument(node.getProperty("title").toString() , DB));
			}
			tx.success();
			tx.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (tx != null){
				tx.failure();
				tx.close();
			}
			e.printStackTrace();
		}
		return new NodeContainer(allpages, allpeople, allcities, allmonuments);
	}
	
	/**
	 * Searchrequest for a node in DB by ID
	 * @param DB Neo4j GraphDatabaseService
	 * @param ID requested node ID
	 * @return custom NodeContainer Object
	 */
	public static NodeContainer getContainerFromID(GraphDatabaseService DB, Long ID){
		Set<Page> allpages= new HashSet<>();
		Set<Person> allpeople= new HashSet<>();
		Set<City> allcities= new HashSet<>();
		Set<Monument> allmonuments= new HashSet<>();
		Transaction tx = null;
		try {
			tx = DB.beginTx();
			Node node = DB.getNodeById(ID);
			if (node.hasLabel(Label.label("Page"))){
				allpages.add(Pagefactory.makePage(DB, node));
			}
			if (node.hasLabel(Label.label("Person"))){
				allpeople.add(new Person(node.getProperty("title").toString() , DB));
			}
			if (node.hasLabel(Label.label("City"))){
				allcities.add(new City(node.getProperty("title").toString() , DB));
			}
			if (node.hasLabel(Label.label("Monument"))){
				allmonuments.add(new Monument(node.getProperty("title").toString() , DB));
			}
			tx.success();
			tx.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (tx != null){
				tx.failure();
				tx.close();
			}
			e.printStackTrace();
		}
		return new NodeContainer(allpages, allpeople, allcities, allmonuments);
	}
}