// File handling
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;
//String works
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//logger
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

//html parser
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

//neo4j
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
/**
 * The Pagefactory service-class gets an html-data-file and builds various
 * Page-Nodes and imports them into a Neo4j Database.
 * Pages need to be separated by the symbol "�".
 * Page-Nodes can then be instanced from the Database.
 * @author Andreas Berger, Sebastian Witt, Dominik Albert, Ines Abou Saif
 *
 */

//A top-level Java class mimicking static class behavior
public final class Pagefactory {
	
	
	private static final Logger log4j = LogManager.getLogger("Pagefactory");
	private static Label label = Label.label("Page");
	private static GraphDatabaseService db;
    
	private Pagefactory() { // private constructor
    }
	
	
	/**
	 * imports special formatted html dump to Neo4j Graphdatabase Nodes
	 * @param DB the Neo4j GraphDatabaseService
	 * @param htmldump the path to the html dump file
	 * @param dbdirectory the path to the database
	 */
    public static void htmlImport(GraphDatabaseService DB, String htmldump, String dbdirectory) {
    	//logging
    	log4j.info("import from: " + htmldump);
    	log4j.info("import to: " + dbdirectory);
    	
    	// precount pages
    	int totalcount = pageCount(htmldump);
    			
        //Database 
        db = DB;
        
        // IO
    	BufferedReader r;
    	InputStreamReader streamreader;
    	FileInputStream stream;
    	File file;
    	try {
    		file = new File(htmldump);
    		stream = new FileInputStream(file);
    		streamreader = new InputStreamReader(stream, Charset.forName("UTF-8"));
			r = new BufferedReader(streamreader);
			String line = null;
			String htmlpage = null;
			Document pagedoc = null;
			
			//Pattern toplvlpattern = Pattern.compile("([\\d]+)\\s([\\d]+)\\s([\\w\\s]+)");
			//Expression above doesn't accept � � � and punctuation. Temporary fix below
			//TODO expression [\\d\\D]+ might be dangerous as it literately accepts EVERYTHING
			Pattern toplvlpattern = Pattern.compile("([\\d]+)\\s([\\d]+)\\s([\\d\\D]+)");
			Matcher matcher = null;
			int pagecount = 0,starcount = 0, nodecount=0, errorcount=0;
			
			log4j.info("parsing - this might take a while...");
			//read entire file line by line create new Page object and add to Set.
			while ((line = r.readLine()) != null){
				// "\u00A4" = unicode for the currency sign
				if (line.contains("\u00A4")){ 
					starcount += 1;
				}
				//make clean string for parser
				htmlpage = htmlpage + line.trim().replaceAll("\u00A4", "");
				
				//every even star we create a new page
				//htmlpage should now contain a complete html string
				if (starcount == 2){
					starcount = 0;
					pagecount += 1;
					if (pagecount % 500 == 0){
						log4j.info("parsing page# " + pagecount);
					}else{
					    log4j.trace("parsing page# " + pagecount);
					}
					//TODO just for debugging - remove me
					//Set<String> categories = Linkextractor.getCategories(htmlpage);
					//Set<String> articles = Linkextractor.getArticles(htmlpage);
					pagedoc = Jsoup.parse(htmlpage);
					String toplvlstring = pagedoc.body().childNode(0).toString();
					toplvlstring = toplvlstring.trim();
					log4j.debug("bodyToplvlString:\n" + toplvlstring);
					matcher = toplvlpattern.matcher(toplvlstring);
					Page newpage = null;
					while (matcher.find()){
						
						String pageID = matcher.group(1);
					    String namespaceID = matcher.group(2);
					    String title = matcher.group(3);
					    
					    Boolean check = Database.checkNode(db, Label.label("Page"), namespaceID, title);
					    // no already existing node
						if (check == false){
							// false on error
						    if (Database.addnewPage(db, pageID, namespaceID, title, htmlpage) != true){
						    	 errorcount += 1;
						     }
					    }else{
				    	    nodecount +=1;
				        }
					}
					log4j.trace("finished page# " + pagecount);
					//reset for next page
					htmlpage = null;
				}
			}
			log4j.info("finished!");
			log4j.info("found  " + pagecount + " html pages:\n New pages: " + (pagecount-nodecount-errorcount) + "\n existing pages: " + nodecount + "\n errors: " + errorcount  );
			
			r.close();
			streamreader.close();
			stream.close();
		} catch (IOException e) {
			log4j.error("IOException: I probably couldn't find or access the file");
			e.printStackTrace();
		}finally{
			if (db != null){db.shutdown();}
		}
    	
    	//return pageSet;
    }
    
    /**creates a Page entitiy from Database
     * 
     * @param pagenode The Neo4j Node from a Neo4j Database containing all relevant page data
     * @param db Neo4j GraphDatabaseService
     * @return The Page Object or null if it doesnt exist
     */
    public static Page makePage(GraphDatabaseService db, Node pagenode){
    	if(pagenode != null){
    		Transaction tx = db.beginTx();
    		Page newpage = null;
    		try{
    			newpage = new Page(db, pagenode.getId(), Integer.parseInt(pagenode.getProperty("pageID").toString()),
        	    Integer.parseInt(pagenode.getProperty("namespaceID").toString()),
        	    pagenode.getProperty("title").toString());
    			tx.success();
    		}catch(Exception e){
    			tx.failure();
    			newpage = null;
    			
    		}finally{
    			if (tx != null) {tx.close();}
    		}
    		return newpage;
    	    
    	}else{
    		return null;
    	}
    }
    /**
     * count pages in a formatted htmldump - helper function to estimate time till finish
     * @param htmldump html String to process
     * @return number of correctly formatted pages as int
     */
    public static int pageCount(String htmldump){
    	// IO
    	log4j.info("counting pages...");
    	BufferedReader r = null;
    	InputStreamReader streamreader = null;
    	FileInputStream stream = null;
    	File file = null;
    	
    	int pagecount = 0,starcount = 0;
    	try {
    		file = new File(htmldump);
    		stream = new FileInputStream(file);
    		streamreader = new InputStreamReader(stream, Charset.forName("UTF-8"));
			r = new BufferedReader(streamreader);
			String line = null;
			
			//read entire file line by line create new Page objects and add to DB
			while ((line = r.readLine()) != null){
				if (line.contains("\u00A4")){
					starcount += 1;
				}				
				//every even star we create a new page
				//htmlpage should now contain a complete html string
				if (starcount == 2){
					starcount = 0;
					pagecount += 1;
					if (pagecount % 5000 == 0){
						log4j.info("pagecount: " + pagecount);
					}
			    }
			}
		}catch(Exception e){
			e.printStackTrace();
		}
    	log4j.info("total pagecount: " + pagecount);
    	return pagecount;
    }
    
}