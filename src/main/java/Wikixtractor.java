



//logger
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
// neo4j
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Mainpage with command-oriented-console-menu, that awaits commands like
 * help, import, export, exit.
 * when run with parameters will act like service-class 
 * @author Andreas Berger, Sebastian Witt, Dominik Albert, Ines Abou Saif
 *
 */

public class Wikixtractor {

	private static Logger log4j;
	private static Scanner input = new Scanner(System.in);
	private static Set<Page> pages = null;
	private static String rootfolder;
	private static String command;
	private static String htmldump;
	private static String dbdirectory;
	private static String namespaceID;
	private static String title;
	
	private static GraphDatabaseService db;
	
	
	
	
	/**
	 * needs to be called with parameters - will run as service class.
	 * Available parameters for the service class:
	 * @param args <CODE>reset DB-Directory</CODE>
	 * >||< [delete database in directory and rebuilt it empty]
	 * @param args <CODE>importhtml DB-Directory HTML-Input-File</CODE>
	 * >||< [import given html-dump file into database]
	 * @param args <CODE>exportxml DB-Directory XML-Output-File</CODE>
	 * >||< [export database to XML file]
	 * @param args <CODE>categorylinks DB-Directory</CODE>
	 * >||< [extract category links and add them to given database]
	 * @param args <CODE>articlelinks DB-Directory</CODE>
	 * >||< [extract links between articles and add them to the given database]
	 * @param args <CODE>entitylinks DB-Directory</CODE>
	 * >||< [creates entities and links them to respective page]
	 * @param args <CODE>pageinfo DB-Directory namespaceID page-title</CODE>
	 * >||< [display additional information for the selected page]
	 * @param args <CODE>createdb DB-Directory HTML-Input-File</CODE>
	 * >||< [runs {reset, importhtml, categorylinks, articlelinks, entitylinks} for complete ready database]
	 */
	public static void main(String[] args) {		
		// Milestone 1
		
		log4j = LogManager.getLogger(Wikixtractor.class);
		log4j.trace("initiating: " + Wikixtractor.class);
		//get root folder of the code
		try {
			rootfolder = Wikixtractor.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			rootfolder = URLDecoder.decode(rootfolder, "UTF-8");  // rootfolder.getPath().replace("%20"," ");  // backup in case this one fails.
			rootfolder = rootfolder.substring(0, rootfolder.lastIndexOf("/") + 1);
			log4j.trace("filepath: " + rootfolder);
		} catch (UnsupportedEncodingException e1) {
			rootfolder = "";
			log4j.error("rootfolder URL could not be encoded to path");
			e1.printStackTrace();
		}
		
		
		// Kommandozeilenparameter
		if (args.length >= 2){
			command = args[0];
			dbdirectory = args[1];
			//dbdirectory = rootfolder + dbdirectory; //TODO this is an overwrite for debugging purpose
			if (args.length == 3) {
				htmldump = args[2];
				//htmldump = rootfolder + "input/" + htmldump; //TODO this is an overwrite for debugging purpose
			}
			if (args.length == 4) {
				namespaceID = args[2];
				title = args[3];	
			}
			
			String commandline = "";
			for (String param : args){
				commandline = commandline +" " + param;
			}
			log4j.debug("startup params:\n" + commandline);
			if (command.equals("reset")){
				log4j.info("Hello Master - resetting database at: " + dbdirectory);
				Database.createDB(dbdirectory);
			}else{
				db = Database.openDB(dbdirectory);
			    if ((db != null)&&(db.isAvailable(1000))){
			    	log4j.info("Database: waiting for index to come online...");
			    	Transaction tx = db.beginTx();
			    	db.schema().awaitIndexesOnline(5, TimeUnit.MINUTES);
			    	tx.success();
                    tx.close();
                    log4j.info("Database:...index is online!");
			    	
                    switch (command){
                        case "importhtml":
                            log4j.info("Hello Master - importing pages from: '" + htmldump + "' to database at: '" + dbdirectory + "'");
                            Pagefactory.htmlImport(db, htmldump, dbdirectory);
                            break;
                        case "exportxml":
                            log4j.info("Hello Master - exporting pages");
                            NodeContainer allnodes = Database.allNodes(db);
                            try {
                            	PageExport.toXML(htmldump, allnodes, db);
                            } catch (IOException e) {
                            	// TODO Auto-generated catch block
                            	e.printStackTrace();
                            }
                            break;
                        case "categorylinks":
                            log4j.info("Hello Master - extracting categories from database at: '" + dbdirectory + "'");
                            Database.addCategoryLinks(db);
                            break;
                        case "articlelinks":
                            log4j.info("Hello Master - extracting articles from database at: '" + dbdirectory + "'");
                            Database.addArticleLinks(db);
                            break;
                        case "pageinfo":
                            log4j.info("Hello Master - gathering page info for (" + namespaceID + ", "+ title + ") from database at: '" + dbdirectory + "'");
                            Page.printPage(title, namespaceID, db);
                            break;
                        case "entitylinks":
                        	Database.addEntityLinks(db);
                            break;
                        case "executetasks":
                        	TaskScheduler.executeTasks(db, htmldump);
                        	break;
                        case "createdb":
                        	log4j.info("Hello Master - resetting database at: " + dbdirectory);
            				Database.createDB(dbdirectory);
            				log4j.info("importing pages from: '" + htmldump + "' to database at: '" + dbdirectory + "'");
                            Pagefactory.htmlImport(db, htmldump, dbdirectory);
                            log4j.info("extracting categories from database at: '" + dbdirectory + "'");
                            Database.addCategoryLinks(db);
                            log4j.info("extracting articles from database at: '" + dbdirectory + "'");
                            Database.addArticleLinks(db);
                            log4j.info("extracting entities from database at: '" + dbdirectory + "'");
                            Database.addEntityLinks(db);
                        	break;
                        case "runserver":
                        	WebInterface ws = new WebInterface(db, 40407);
                        	Scanner user_input = new Scanner( System.in );
                        	String uinput = "";
                        	while (true){
                        		System.out.println("Server running - type 'exit' to stop the server: ");
                        		uinput = user_input.nextLine();
                        		if (uinput.toLowerCase().equals("exit")){
                        			System.exit(0);
                        		}
                        	}
                        default:
                            log4j.error("wrong input parameter, shutting down");
                            break;
			        }


			    }else{
			        log4j.error("no Database at" + dbdirectory + "\nplease use reset <Database> to create a new Database");
		        }	
			}
			
			if (db != null){
				db.shutdown();
			}
		}else{
			log4j.error("wrong input parameter, shutting down");	
	    }
	}




	public static String getRootfolder() {
		return rootfolder;
	}
}
