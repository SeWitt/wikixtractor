import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

/** Class for representing a "City" Labeled Nodes from the Database as an City Object.
 * Specifically for printing a Node to the Console.
 * 
 * @author Sebastian Witt
 *
 */

public class City extends Entity{
	private String sourceID;
	private Long ID;
	private String title;
	private String cityName;
	private String country;
	private String population;
	private String foundingYear;
	private Node node;
	private static GraphDatabaseService db;
	private Set<Entity> cityLinks = new HashSet<>();
	
	/**Constructor for City Objects
	 * The Title of the Wiki Page is used to find all Informations in the Database using Cypher Querries.
	 * Every Node needs Properties with the exact same names, as the class attributes without the link Set plus the title Property.
	 * 
	 * @param title title of City Page as String
	 * @param DB neo4j GraphDatabaseService
	 */
	public City(String title, GraphDatabaseService DB) {
		db = DB;
		this.title = title;
		this.cityName = Query.getCityName(title, DB);
		this.country = Query.getCityCountry(title, DB);
		this.population = Query.getCityPopulation(title, DB);
		this.foundingYear = Query.getCityFoundingYear(title, DB);
		this.cityLinks = Query.getCityLinks(title, DB);
		Transaction tx = db.beginTx();
		this.sourceID = Query.getSourceID(title, DB, "City");
		this.ID = this.getNode().getId();
		tx.success();
		tx.close();
	}
	
	public String getSourceID() {
		return sourceID;
	}

	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	/** Method for Printing a City Object to the Console
	 * 
	 */
	@Override
	public void print(){
		System.out.println("Stadtname: " + this.cityName);
		System.out.println("Land: " + this.country);
		System.out.println("Bevölkerungszahl: " + this.population);
		System.out.println("Gründungsjahr: " + this.foundingYear);
		for(Entity a: cityLinks){
			a.getName();
		}
	}
	
	/**Method for returning the Name of a Entity
	 * 
	 */
	@Override
	public String getName(){
		return this.cityName;
	}
	public String getCityName() {
		return cityName;
	}

	public String getTitle() {
		return title;
	}
	
	public String getCountry() {
		return country;
	}

	public String getPopulation() {
		return population;
	}

	public String getFoundingYear() {
		return foundingYear;
	}

	public Set<Entity> getCityLinks() {
		return cityLinks;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityLinks == null) ? 0 : cityLinks.hashCode());
		result = prime * result + ((cityName == null) ? 0 : cityName.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((foundingYear == null) ? 0 : foundingYear.hashCode());
		result = prime * result + ((population == null) ? 0 : population.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (cityLinks == null) {
			if (other.cityLinks != null)
				return false;
		} else if (!cityLinks.equals(other.cityLinks))
			return false;
		if (cityName == null) {
			if (other.cityName != null)
				return false;
		} else if (!cityName.equals(other.cityName))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (foundingYear == null) {
			if (other.foundingYear != null)
				return false;
		} else if (!foundingYear.equals(other.foundingYear))
			return false;
		if (population == null) {
			if (other.population != null)
				return false;
		} else if (!population.equals(other.population))
			return false;
		return true;
	}
	
	
	public Node getNode(){
		return Database.getNode( db, Label.label("City"), "0", this.title);
	}
	public void grabNode(){
		this.node = Database.getNode( db, Label.label("City"), "0", this.title);
	}
	public void dumpNode(){
		this.node = null;
	}

}
