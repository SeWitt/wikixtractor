import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.GraphDatabaseService;

import static spark.Spark.*;

/**
 * a Web interface to access data from our Neo4j Database
 * @author andreas
 *
 */
public class WebInterface {
	
	private static final Logger log4j = LogManager.getLogger("Webinterface");
	
	/**
	 * Initialize the webinterface
	 * @param DB Neo4j GraphDatabaseService
	 * @param port the port on which the server will listen
	 */
	public WebInterface(GraphDatabaseService DB, int port){
		log4j.info("initializing webserver on port: " + port);
		spark.Spark.
		
		port(port);
		get("/hello", (req, res) -> "Hello World: " + "test");
		
		//suche http://localhost:40407/page?title=Frankfurt
		get("/page", (req, res) -> {
			String title = req.queryMap("title").value();
			log4j.info("request for page title: '"+ title + "' on port: "+ port);
			String output = "The requested pages with title " + title + ":\n";
			String raw = PageExport.toWeb(Database.getPageContainerFromTitle(DB, title), DB);
			String xmlcontent = escapeHTML(raw);
			xmlcontent = "<pre>" + xmlcontent.replaceAll("/n", "<br>") + "</pre>";
			output = output + "<br>" + xmlcontent + "<br>";
			log4j.trace(output);
			return output;
		});
		
		//suche http://localhost:40407/person?name=Hans
		get("/person", (req, res) -> {
			String name = req.queryMap("name").value();
			log4j.info("request for person name: '"+ name + "' on port: "+ port);
			String output = "People with name " + name + ":\n";
			String raw = PageExport.toWeb(Database.getPersonContainerFromName(DB, name), DB);
			String xmlcontent = escapeHTML(raw);
			xmlcontent = "<pre>" + xmlcontent.replaceAll("/n", "<br>") + "</pre>";
			output = output + "<br>" + xmlcontent + "<br>";
			log4j.trace(output);
			return output;
		});
		
		//suche http://localhost:40407/person?surname=Mayer
		get("/person", (req, res) -> {
			String surname = req.queryMap("surname").value();
			log4j.info("request for person surname: '"+ surname + "' on port: "+ port);
			String output = "People with surname " + surname + ":\n";
			String raw = PageExport.toWeb(Database.getPersonContainerFromSurname(DB, surname), DB);
			String xmlcontent = escapeHTML(raw);
			xmlcontent = "<pre>" + xmlcontent.replaceAll("/n", "<br>") + "</pre>";
			output = output + "<br>" + xmlcontent + "<br>";
			log4j.trace(output);
			return output;
		});
		
		//suche http://localhost:40407/city?name=Frankfurt
		get("/city", (req, res) -> {
			String name = req.queryMap("name").value();
			log4j.info("request for city name: '"+ name + "' on port: "+ port);
			String output = "Cities with name " + name + ":\n";
			String raw = PageExport.toWeb(Database.getCityContainerFromName(DB, name), DB);
			String xmlcontent = escapeHTML(raw);
			xmlcontent = "<pre>" + xmlcontent.replaceAll("/n", "<br>") + "</pre>";
			output = output + "<br>" + xmlcontent + "<br>";
			log4j.trace(output);
			return output;
		});
		
		//suche http://localhost:40407/monument?name=Kriegerdenkmal
		get("/monument", (req, res) -> {
			String name = req.queryMap("name").value();
			log4j.info("request for monument name: '"+ name + "' on port: "+ port);
			String output = "Monuments with name " + name + ":\n";
			String raw = PageExport.toWeb(Database.getMonumentContainerFromName(DB, name), DB);
			String xmlcontent = escapeHTML(raw);
			xmlcontent = "<pre>" + xmlcontent.replaceAll("/n", "<br>") + "</pre>";
			output = output + "<br>" + xmlcontent + "<br>";
			log4j.trace(output);
			return output;
		});
		
		//id abfrage http://localhost:40407/object?id=1
		get("/object", (req, res) -> {
			Long id = Long.parseLong(req.queryMap("id").value());
			log4j.info("request for page id: "+ id + "on port: "+ port);
			String output = "The requested Page with id " + id + ":\n";
			String raw = PageExport.toWeb(Database.getContainerFromID(DB, id), DB);
			String xmlcontent = escapeHTML(raw);
			xmlcontent = "<pre>" + xmlcontent.replaceAll("/n", "<br>") + "</pre>";
			output = output + "<br>" + xmlcontent + "<br>";
			log4j.trace(output);
			return output;
		});
		
		
	}
	
	
	public void exit(){
		System.exit(0);
	}
	
	
	/**
	 * convert xml string to raw text in html
	 * Source: http://stackoverflow.com/questions/655746/is-there-a-jdk-class-to-do-html-encoding-but-not-url-encoding
	 * @param s String to convert
	 * @return reformatted string
	 */
	public static String escapeHTML(String s) {
	    StringBuilder out = new StringBuilder(Math.max(16, s.length()));
	    for (int i = 0; i < s.length(); i++) {
	        char c = s.charAt(i);
	        if (c > 127 || c == '"' || c == '<' || c == '>' || c == '&') {
	            out.append("&#");
	            out.append((int) c);
	            out.append(';');
	        } else {
	            out.append(c);
	        }
	    }
	    return out.toString();
	}

}
