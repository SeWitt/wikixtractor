import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

/** Class for representing a "Person" Labeled Nodes from the Database as an Person Object.
 * Specifically for printing a Node to the Console.
 * 
 * @author Sebastian Witt
 *
 */
public class Person extends Entity{
	private Long ID;
	private String sourceID;
	private String title;
	private String name;
	private String surname;
	private String birthname;
	private String birthday;
	private String dayOfDeath;
	private String birthplace;
	private String lastLocation;
	private Node node;
	private static GraphDatabaseService db;
	private Set<Entity> personLinks = new HashSet<>();
	
	/**Constructor for Person Objects
	 * The Title of the Wiki Page is used to find all Informations in the Database using Cypher Querries.
	 * Every Node needs Properties with the exact same names, as the class attributes without the link Set plus the title Property.
	 * 
	 * @param title ...
	 * @param DB neo4j GraphDatabaseService
	 */
	public Person(String title, GraphDatabaseService DB) {
		db = DB;
		this.title = title;
		this.name = Query.getPersonName(title, DB);
		this.surname = Query.getSurname(title, DB);
		this.birthname = Query.getPersonBirthname(title, DB);
		this.birthday = Query.getPersonBirthday(title, DB);
		this.dayOfDeath = Query.getPersonDayofDeath(title, DB);
		this.birthplace = Query.getPersonBirthplace(title, DB);
		this.lastLocation = Query.getPersonLastLocation(title, DB);
		this.personLinks = Query.getPersonLinks(title, DB);
		this.sourceID = Query.getSourceID(title, DB, "Person");
		Transaction tx = db.beginTx();
		//TODO for some reason monuments can call this with faulty parameters... >_>
		try{
			this.ID = this.getNode().getId();
		}catch(Exception e){
			this.ID = (long) 0;
		}
		tx.success();
		tx.close();
	}
	
	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getSourceID() {
		return sourceID;
	}

	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}

	public String getSurname() {
		return surname;
	}

	public String getTitle() {
		return title;
	}
	
	public String getBirthname() {
		return birthname;
	}

	public String getBirthday() {
		return birthday;
	}

	public String getDayOfDeath() {
		return dayOfDeath;
	}

	public String getBirthplace() {
		return birthplace;
	}

	public String getLastLocation() {
		return lastLocation;
	}

	public Set<Entity> getPersonLinks() {
		return personLinks;
	}

	/** Method for Printing a Person Object to the Console
	 * 
	 */
	@Override
	public void print(){
		System.out.println("Vorname: " + this.name);
		System.out.println("Nachname: " + this.surname);
		System.out.println("Geburtsname: " + this.birthname);
		System.out.println("Geburtstag: " + this.birthday);
		System.out.println("Todestag: " + this.dayOfDeath);
		System.out.println("Geburtsort: " + this.birthplace);
		System.out.println("Letzter Wohnort: " + this.lastLocation);
		for(Entity a: personLinks){
			a.getName();
		}
	}
	
	/**Method for returning the Name of a Entity
	 * 
	 */
	@Override
	public String getName(){
		return (this.name + " " + this.surname);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((birthname == null) ? 0 : birthname.hashCode());
		result = prime * result + ((birthplace == null) ? 0 : birthplace.hashCode());
		result = prime * result + ((dayOfDeath == null) ? 0 : dayOfDeath.hashCode());
		result = prime * result + ((lastLocation == null) ? 0 : lastLocation.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (birthname == null) {
			if (other.birthname != null)
				return false;
		} else if (!birthname.equals(other.birthname))
			return false;
		if (birthplace == null) {
			if (other.birthplace != null)
				return false;
		} else if (!birthplace.equals(other.birthplace))
			return false;
		if (dayOfDeath == null) {
			if (other.dayOfDeath != null)
				return false;
		} else if (!dayOfDeath.equals(other.dayOfDeath))
			return false;
		if (lastLocation == null) {
			if (other.lastLocation != null)
				return false;
		} else if (!lastLocation.equals(other.lastLocation))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
	
	
	public Node getNode(){
		return Database.getNode( db, Label.label("Person"), "0", this.title);
	}
	public void grabNode(){
		this.node = Database.getNode( db, Label.label("Person"), "0", this.title);
	}
	public void dumpNode(){
		this.node = null;
	}

}
