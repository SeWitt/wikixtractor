import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;

public class TaskScheduler {
	
	private static final Logger log4j = LogManager.getLogger("TaskScheduler");
	private static GraphDatabaseService db;
	private static int html_inserted;
	private static int catlink_inserted;
	private static int artlink_inserted;
	private static int entity_inserted;
	
	public static void executeTasks(GraphDatabaseService DB, String htmlpage){
		
	}
	
	public static void check_inserted(String ToCheck){
		switch (ToCheck){
        case "HTMLDumpImport":
            log4j.info("No preconditions");
           // String htmldump = Wikixtractor.getHtmldump();
           // String dbdirectory = Wikixtractor.getDbdirectory();
           // Pagefactory.htmlImport(db, htmldump, dbdirectory);
            break;
        case "CategoryLinkExtraction":
            log4j.info("Checking for preconditions : html_inserted");
            if (html_inserted == 10){
            	Database.addCategoryLinks(db);}
            else 
            	log4j.info("Preconditions not met");
            break;
        case "ArticleLinkExtraction":
            log4j.info("Checking for preconditions : html_inserted ");
            if (html_inserted == 10){
            	Database.addArticleLinks(db);}
            else
            	log4j.info("Preconditions not met");
            break;
        case "EntityBaseExtraction":
            log4j.info("Checking for preconditions : artlink_inserted , catlink_inserted");
            if ((getArtlink_inserted() == 10) && (getCatlink_inserted() == 10)) {
            	Database.addEntityLinks(db);
            }
            else
            	log4j.info("preconditions not met");
            break;
//        case "PersonExtraction":
//            log4j.info("Checking for preconditions : entity_inserted");
//            if (getEntity_inserted() == 10){
            	//Page.printPage(title, namespaceID, db);

//        case "PersonExtraction":
//            log4j.info("Checking for preconditions : entity_inserted");
//            if (entity_inserted == 10){
//            	//Page.printPage(title, namespaceID, db);
//            }
//            else
//            	log4j.info("Preconditions not met");
//            break;
//        case "CityExtraction":
//            log4j.info("Checking for preconditions : entity_inserted");
//            if (entity_inserted == 10){
//            	//Page.printPage(title, namespaceID, db);
//            }
//            else
//            	log4j.info("Preconditions not met");
//            break;
//        case "MonumentExtraction":
//            log4j.info("Checking for preconditions : entity_inserted");
//            if (entity_inserted == 10){
//            	//Page.printPage(title, namespaceID, db);
//            }
//            else
//            	log4j.info("Preconditions not met");
//            break;
        default :
        	log4j.info("Wrong input");
        	break;
		}
	}	

	public static int getCatlink_inserted() {
		return catlink_inserted;
	}

	public static void setCatlink_inserted(int catlink_inserted) {
		TaskScheduler.catlink_inserted = catlink_inserted;
	}

	public static int getArtlink_inserted() {
		return artlink_inserted;
	}

	public static void setArtlink_inserted(int artlink_inserted) {
		TaskScheduler.artlink_inserted = artlink_inserted;
	}

	public static int getEntity_inserted() {
		return entity_inserted;
	}

	public static void setEntity_inserted(int entity_inserted) {
		TaskScheduler.entity_inserted = entity_inserted;
	}

}
