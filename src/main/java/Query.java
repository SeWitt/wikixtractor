import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Result;

/**Class for Queries to the Database
 * There are Querries for Page, Person, Monument and City Entitys
 * 
 * @author Sebastian Witt
 *
 */


public class Query {
	private static Logger log4j = LogManager.getLogger(Query.class);

	/**	Method for fetching and returning all categories of a Node
	 * 
	 * @param title of the Node looked at
	 * @param namespaceID of the Node looked at
	 * @param db The Database
	 * @return Set<String> Set of the fetched Categories
	 */
	
	protected static Set<String> getPageCategories(String title, String namespaceID, GraphDatabaseService db){
		log4j.info("Trying to get Categories out of " + title);
		Result nodes = db.execute("MATCH ((n:Page)-[:has_category]->(m:Page)) WHERE (n.title = \"" + title + "\" AND n.namespaceID = \"" + namespaceID + "\") RETURN m.title");
		ResourceIterator<String> stringNodes = nodes.columnAs("m.title");
		return resourceIteratorToSet(stringNodes);
	}
	
	/** Method for fetching and returning all indirect Categories of a Node
	 * 
	 * @param title of the Node looked at
	 * @param namespaceID of the Node looked at
	 * @param db ...
	 * @return Set<String> Set of the fetched Categories
	 */
	protected static Set<String> getPageIndirectCategories(String title, String namespaceID, GraphDatabaseService db){
		log4j.info("Get indirect Categories from " + title);
		Result nodes = db.execute("MATCH ((n:Page)-[:has_category*]->(m:Page)) WHERE (n.title = \"" + title + "\" AND n.namespaceID = \"" + namespaceID + "\")RETURN m.title");
		ResourceIterator<String> stringNodes = nodes.columnAs("m.title");
		return resourceIteratorToSet(stringNodes);
	}
	
	/** Method for fetching and returning all Articles directly linked from the node given by the Properties Title and NamespaceID
	 * 
	 * @param title of the Node looked at
	 * @param namespaceID of the Node looked at
	 * @param db
	 * @return Set<String> Set of the fetched Articles
	 */ 
	protected static Set<String> outgoingArticles(String title, String namespaceID, GraphDatabaseService db){
		log4j.info("Get all Articles linked from " + title);
		Result nodes = db.execute("MATCH ((n:Page)-[:has_friend]->(m:Page)) WHERE (n.title = \"" + title + "\" AND n.namespaceID = \"" + namespaceID + "\")RETURN m.title");
		ResourceIterator<String> stringNodes = nodes.columnAs("m.title");
		return resourceIteratorToSet(stringNodes);
	}
	
	/** Method for fetching and returning all Articles with links to the given Node
	 * 
	 * @param title of the Node looked at
	 * @param namespaceID of the Node looked at
	 * @param db 
	 * @return Set of Articles linking to the given Node
	 */
	protected static Set<String> incomingArticles(String title, String namespaceID, GraphDatabaseService db){
		log4j.info("Get all Incoming Links from other Articles to " + title);
		Result nodes = db.execute("MATCH ((n:Page)-[:has_friend]-(m:Page)) WHERE (n.title = \"" + title + "\" AND n.namespaceID = \"" + namespaceID + "\")RETURN m.title");
		ResourceIterator<String> stringNodes = nodes.columnAs("m.title");
		return resourceIteratorToSet(stringNodes);
	}
	
	/** Method for converting ResourceIterator<> to Set<>
	 * 
	 * @param ri
	 * @return Set of the Elements of ri
	 */
	private static Set<String> resourceIteratorToSet(ResourceIterator<String> ri){
		Set<String> resourceSet = new HashSet<>();
		while(ri.hasNext()){
			resourceSet.add(ri.next());
		}
		return resourceSet;
	}
	
	/** Method for returning the PageID of the Node given by the Properties Title and NamespaceID
	 * 
	 * @param title
	 * @param namespaceID
	 * @param db
	 * @return
	 */
	protected static int getPageID(String title, String namespaceID, GraphDatabaseService db) {
		log4j.info("Get the PageID from " + title);
		Result nodes = db.execute("MATCH (n:Page) WHERE (n.title = \"" + title + "\" AND n.namespaceID = \"" + namespaceID + "\") RETURN n.pageID");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.pageID");
		if(stringNodes.hasNext()){
			return Integer.parseInt(stringNodes.next());
			}
		else{
			return 0;
		}
	}
	
	/** Method for returning the Vorname from a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return name of person as String
	 */
	public static String getPersonName(String title, GraphDatabaseService db) {
		log4j.info("Get Name from " + title);
		Result nodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + title + "\")RETURN n.name");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.name");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}


	/** Method for returning the Surname from a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return surname of Person as String
	 */
	public static String getSurname(String title, GraphDatabaseService db) {
		log4j.info("Get Surname from " + title);
		Result nodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + title + "\")RETURN n.surname");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.surname");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}
	
	/** Method for returning the Birthname from a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return birthname of person as String
	 */
	protected static String getPersonBirthname(String title, GraphDatabaseService db) {
		log4j.info("Get Birthname from " + title);
		Result nodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + title + "\")RETURN n.birthname");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.birthname");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}
		
	/** Method for returning the Birthday from a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return Birthday of Person as String
	 */
	protected static String getPersonBirthday(String title, GraphDatabaseService db) {
		log4j.info("Get Birthday from " + title);
		Result nodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + title + "\")RETURN n.birthday");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.birthday");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the day of Death from a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return day of death as String
	 */
	protected static String getPersonDayofDeath(String title, GraphDatabaseService db) {
		log4j.info("Get Day of Death from " + title);
		Result nodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + title + "\")RETURN n.dayOfDeath");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.dayOfDeath");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}


	/** Method for returning the Birthplace of a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return birthplace of person as String
	 */
	protected static String getPersonBirthplace(String title, GraphDatabaseService db) {
		log4j.info("Get birthplace from " + title);
		Result nodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + title + "\")RETURN n.birthplace");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.birthplace");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the last known Loaction of a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return lastlocation of person as String
	 */
	protected static String getPersonLastLocation(String title, GraphDatabaseService db) {
		log4j.info("Get last Residence of " + title);
		Result nodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + title + "\")RETURN n.lastLocation");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.lastLocation");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning a Set of Links to other Entitys from a Person using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return links from person to other entitties titles as String
	 */
	protected static Set<Entity> getPersonLinks(String title, GraphDatabaseService db) {
		log4j.info("Get all Entity Links from " + title);
		Result personNodes = db.execute("MATCH ((n:Person)-[:entity_link]->(m:Person)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		Result monumentNodes = db.execute("MATCH ((n:Person)-[:entity_link]->(m:Monument)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		Result cityNodes = db.execute("MATCH ((n:Person)-[:entity_link]->(m:City)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		ResourceIterator<String> personNodesAsString = personNodes.columnAs("m.title");
		ResourceIterator<String> monumentNodesAsString = monumentNodes.columnAs("m.title");
		ResourceIterator<String> cityNodesAsString = cityNodes.columnAs("m.title");
		Set<Entity> personLinks = new HashSet<>();
		while(personNodesAsString.hasNext()){
			personLinks.add(new Person(personNodesAsString.next(), db));
		}
		while(monumentNodesAsString.hasNext()){
			personLinks.add(new Monument(monumentNodesAsString.next(), db));
		}
		while(cityNodesAsString.hasNext()){
			personLinks.add(new City(cityNodesAsString.next(), db));
		}
		return personLinks;
	}

	/** Method for returning the Name of a Monument using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return name of Monument as String
	 */
	public static String getMonumentName(String title, GraphDatabaseService db) {
		log4j.info("Get Name of " + title);
		Result nodes = db.execute("MATCH (n:Monument) WHERE (n.title = \"" + title + "\")RETURN n.name");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.name");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the Location of a Monument using its Wiki Title
	 * 
	 * @param monumentName ...
	 * @param db neo4j GraphDatabaseService
	 * @return location of monument as String
	 */
	public static String getMonumentLocation(String monumentName, GraphDatabaseService db) {
		log4j.info("Getting Location of " + monumentName);
		Result nodes = db.execute("MATCH (n:Monument) WHERE (n.title = \"" + monumentName + "\")RETURN n.location");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.location");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the founding Year of a Monument using its Wiki Title
	 * 
	 * @param monumentName - Wiki Titel!
	 * @param db neo4j GraphDatabaseService
	 * @return founding year of monument as String
	 */
	public static String getMonumentYear(String monumentName, GraphDatabaseService db) {
		log4j.info("Getting Year of " + monumentName);
		Result nodes = db.execute("MATCH (n:Monument) WHERE (n.title = \"" + monumentName + "\")RETURN n.buildingYear");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.buildingYear");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the Person represented by the Monument using its Wiki Title
	 * 
	 * @param monumentName ...
	 * @param db GraphDatabaseService
	 * @return honored person by Monument as Person object
	 */
	public static Person getMonumentPerson(String monumentName, GraphDatabaseService db) {
		log4j.info("Get Person represented by " + monumentName);
		Result nodes = db.execute("MATCH (n:Monument) WHERE (n.title = \"" + monumentName + "\")RETURN n.monumentPerson");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.monumentPerson");
		Person p = null;
		// check whether the person exists in db
		while(stringNodes.hasNext()){
			Result personnodes = db.execute("MATCH (n:Person) WHERE (n.title = \"" + stringNodes.next() + "\")RETURN n.title");
			ResourceIterator<String> pstringNodes = nodes.columnAs("n.title");
			while (pstringNodes.hasNext()){
				p = new Person(pstringNodes.next(), db);
			}
		}
		return p;
	}

	/** Method for returning a Set of Entitys linked to the Monument in the Database using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return Set of entities linked to Monument
	 */
	public static Set<Entity> getMonumentLinks(String title, GraphDatabaseService db) {
		log4j.info("Get all Entity Links from " + title);
		Result personNodes = db.execute("MATCH ((n:Monument)-[:entity_link]->(m:Person)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		Result monumentNodes = db.execute("MATCH ((n:Monument)-[:entity_link]->(m:Monument)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		Result cityNodes = db.execute("MATCH ((n:Monument)-[:entity_link]->(m:City)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		ResourceIterator<String> personNodesAsString = personNodes.columnAs("m.title");
		ResourceIterator<String> monumentNodesAsString = monumentNodes.columnAs("m.title");
		ResourceIterator<String> cityNodesAsString = cityNodes.columnAs("m.title");
		Set<Entity> personLinks = new HashSet<>();
		while(personNodesAsString.hasNext()){
			personLinks.add(new Person(personNodesAsString.next(), db));
		}
		while(monumentNodesAsString.hasNext()){
			personLinks.add(new Monument(monumentNodesAsString.next(), db));
		}
		while(cityNodesAsString.hasNext()){
			personLinks.add(new City(cityNodesAsString.next(), db));
		}
		return personLinks;
	}

	/** Method for returning the Name of a City using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return city name as String
	 */
	public static String getCityName(String title, GraphDatabaseService db) {
		log4j.info("Get City Name of " + title);
		Result nodes = db.execute("MATCH (n:City) WHERE (n.title = \"" + title + "\")RETURN n.cityName");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.cityName");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the Country in which the City is using its Wiki Title
	 * 
	 * @param name ...
	 * @param db neo4j GraphDatabaseService
	 * @return country of city as String
	 */
	public static String getCityCountry(String name, GraphDatabaseService db) {
		log4j.info("Get Country of " + name);
		Result nodes = db.execute("MATCH (n:City) WHERE (n.title = \"" + name + "\")RETURN n.country");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.country");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the Population of a City using its Wiki Title
	 * 
	 * @param name ...
	 * @param db neo4j GraphDatabaseService
	 * @return population of city as String
	 */
	public static String getCityPopulation(String name, GraphDatabaseService db) {
		log4j.info("Get Population of " + name);
		Result nodes = db.execute("MATCH (n:City) WHERE (n.title = \"" + name + "\")RETURN n.population");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.population");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning the Year in which the City was founded or its earliest mention using its Wiki Title
	 * 
	 * @param name ...
	 * @param db neo4j GraphDatabaseService
	 * @return founding year of city as String
	 */
	public static String getCityFoundingYear(String name, GraphDatabaseService db) {
		log4j.info("Get Founding Year of " + name);
		Result nodes = db.execute("MATCH (n:City) WHERE (n.title = \"" + name + "\")RETURN n.foundingYear");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.foundingYear");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}

	/** Method for returning all Links from the City to other Entitys using its Wiki Title
	 * 
	 * @param title ...
	 * @param db neo4j GraphDatabaseService
	 * @return Set of Entities linked to this city
	 */
	public static Set<Entity> getCityLinks(String title, GraphDatabaseService db) {
		log4j.info("Get all Entity Links from " + title);
		Result personNodes = db.execute("MATCH ((n:City)-[:entity_link]->(m:Person)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		Result monumentNodes = db.execute("MATCH ((n:City)-[:entity_link]->(m:Monument)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		Result cityNodes = db.execute("MATCH ((n:City)-[:entity_link]->(m:City)) WHERE (n.title = \"" + title + "\") RETURN m.title");
		ResourceIterator<String> personNodesAsString = personNodes.columnAs("m.title");
		ResourceIterator<String> monumentNodesAsString = monumentNodes.columnAs("m.title");
		ResourceIterator<String> cityNodesAsString = cityNodes.columnAs("m.title");
		Set<Entity> personLinks = new HashSet<>();
		while(personNodesAsString.hasNext()){
			personLinks.add(new Person(personNodesAsString.next(), db));
		}
		while(monumentNodesAsString.hasNext()){
			personLinks.add(new Monument(monumentNodesAsString.next(), db));
		}
		while(cityNodesAsString.hasNext()){
			personLinks.add(new City(cityNodesAsString.next(), db));
		}
		return personLinks;
	}
	
	
	/**
	 * return the sourceID of entity node
	 * @param title title of Entity Node
	 * @param db neo4j GraphDatabaseService
	 * @param label String name of Label of Entity node
	 * @return String representation of sourceID
	 */
	public static String getSourceID(String title, GraphDatabaseService db, String label) {
		log4j.info("Get sourceID of " + title);
		Result nodes = db.execute("MATCH (n:" + label +") WHERE (n.title = \"" + title + "\")RETURN n.sourceID");
		ResourceIterator<String> stringNodes = nodes.columnAs("n.sourceID");
		if(stringNodes.hasNext()){
			return stringNodes.next();
			}
		else{
			return "";
		}
	}
}


	