//logger
import org.apache.logging.log4j.Logger;
//neo4j
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;

/**
 * Builds a Page-class with different Methods to
 * categorize the input-data in the following categories :
 * pageID , namespaceID , title , categories.
 * @author Andreas Berger, Sebastian Witt, Dominik Albert, Ines Abou Saif
 *
 */
public class Page {

	private Long ID;
	private int pageID;
	private int namespaceID;
	private String title;
	private String htmlpage;
	private Set<String> categories = new HashSet <>();
	private Set<String> indirectcategories = new HashSet <>();
	private Set<String> incomingarticles = new HashSet <>();
	private Set<String> articles = new HashSet<>();
	private static final Logger log4j = LogManager.getLogger(Page.class);
	//Milestone2
	private static GraphDatabaseService db;
	private Node pagenode;
	
	/** A page Object containing Meta data for xml conversion
	 * @param  pageID unique identifier
	 * @param  namespaceID each title should be unique among its namespaceID
	 * @param  title name of a person or object described in the Page
	 * @param  categories a Set of Strings. 
	 * Object in title can be categorized and grouped with multiple instances
	 */
	Page(GraphDatabaseService DB, Long id, int pageID, int namespaceID, String title){
		
		log4j.trace("initiating: " + Page.class);
		db = DB;
		this.ID = id;
		this.setPageID(pageID);
		this.namespaceID = namespaceID;
		this.title = title;
		this.htmlpage = null; // save resources
	}
	
	/** Constructor for Page Class, with all Informations needed for a Output to the Console.
	 * 
	 * @param title ...
	 * @param namespaceID ...
	 * @param DB neo4j GraphDatabaseService
	 */
	public Page(String title, String namespaceID, GraphDatabaseService DB) {
		log4j.info("Trying to initiate a Page for " + title + " in Namespace: " + namespaceID);
		db = DB;
		this.title = title;
		this.namespaceID = Integer.parseInt(namespaceID);
		this.pageID = Query.getPageID(title, namespaceID, db);
		this.categories = Query.getPageCategories(title, namespaceID, db);
		this.indirectcategories = Query.getPageIndirectCategories(title, namespaceID, db);
		this.incomingarticles = Query.incomingArticles(title, namespaceID, db);
		this.articles = Query.outgoingArticles(title, namespaceID, db);
		
	}
	
	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	/** Method for puting Informations of a Node to the Console. Evoked by Argument pageinfo.
	 * 
	 * @param title the title of the Node in DB
	 * @param namespaceID namespaceID of the page
	 * @param DB neo4j GraphDatabaseService
	 */
	public static void printPage(String title, String namespaceID, GraphDatabaseService DB){
		Page page = new Page(title, namespaceID, DB);
		System.out.println("Title: " + page.getTitle());
		System.out.println("NamespaceID: " +page.getNamespaceID());
		System.out.println("PageID: " + page.getPageID());
		System.out.println("Kategorien:");
		System.out.println(page.getCategories());
		System.out.println("Indirekte Kategorien:");
		System.out.println(page.getIndirectCategories());
		System.out.println("Ausgehende Artikel:");
		System.out.println(page.getArticles());
		System.out.println("Eingehende Artikel:");
		System.out.println(page.getIncomingArticles());
	}
	
	private Set<String> getIncomingArticles() {
		return this.incomingarticles;
	}

	private Set<String> getIndirectCategories() {
		return this.indirectcategories;
	}
	
	// get for Title
	public String getTitle() {
		return this.title;
	}
	

	// Set for Title
	/*
	 * 	public void setTitle(String title) {
		this.title = title;
	}

	 */

	// get for namespaceID
	
	public int getNamespaceID() {
		return this.namespaceID;
	}
	

	// Set for namespaceID
	/*
	 * 	public void setNamespaceID(int namespaceID) {
		this.namespaceID = namespaceID;
	}
	 */
    
	
	
 
	public int getPageID() {
		return this.pageID;
	}
	public void setPageID(int pageID) {
		this.pageID = pageID;
	}
	
	
	public Node getNode(){
		return Database.getNode( db, Label.label("Page"), Integer.toString(this.namespaceID), this.title);
	}
	public void grabNode(){
		this.pagenode = Database.getNode( db, Label.label("Page"), Integer.toString(this.namespaceID), this.title);
	}
	public void dumpNode(){
		this.pagenode = null;
	}
	// Html Document operations
	/**
	 * standard setter method
	 * will first grab data from database if htmldoc is not loaded yet 
	 * @return html Document containing wiki data for respective Page
	 */
	public String getHtmlpage() {
		if (htmlpage == null){
			this.grabNode();
			this.grabHtmldoc();
		}
		return htmlpage;
	}
    // standard setterPageCategories(, namespaceID, db)
	public void setHtmldoc(String htmlpage) {
		this.htmlpage = htmlpage;
	}
	/**
	 * grab html content from Database
	 * remember to dump unneeded html content
	 * when working with large number of pages
	 */
	private void grabHtmldoc(){
		log4j.info("grabbing html from" + this.pagenode);
		Transaction tx = db.beginTx();
		try{
		this.htmlpage = (String) this.pagenode.getProperty("html content");
		tx.success();
		tx.close();
		}catch(Exception e){
			log4j.error("couldnt collect html content");
			tx.failure();
			tx.close();
		}
	}
	/**
	 * dump the html document to free up memory
	 */
	public void dumpHtmldoc(){
		this.htmlpage = null;
		this.pagenode = null;
	}
	
    //stndart getter and setter
	public Set<String> getArticles() {
		return articles;
	}

	public void setArticles(Set<String> articles) {
		this.articles = articles;
	}
	// Category operations
	
	// getter
	public Set<String> getCategories() {
		return this.categories;
	}
	
	// add categories takes either a string or a set of strings 
	public void addCategories(String category) {
		if (category != null){
		    this.categories.add(category);
		}
	}
		
	public void addCategories(Set<String> categories) {
		if (categories != null) {
		    for(String category: categories){
		        this.categories.add(category);
			}
		}
	}
	//Milestone 2 
	//moved link extraction from Pagefactory to here as we now grab the data from DB
	public void grabCategories(){
		Set<String> categories = Linkextractor.getCategories(this.htmlpage);
		this.addCategories(categories);
	}
	
	//to compare 2 Pages hashCode and equals (made by Eclipse Java Neon(01.11.2016))
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.namespaceID;
		result = prime * result + ((this.title == null) ? 0 : this.title.hashCode());
		return result;
	}

	//to compare 2 Pages hashCode and equals (made by Eclipse Java Neon(01.11.2016))
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Page other = (Page) obj;
		if (this.namespaceID != other.getNamespaceID())
			return false;
		if (this.title == null) {
			if (other.getTitle() != null)
				return false;
		} else if (!this.title.equals(other.getTitle()))
			return false;
		return true;
	}
	
	/*
	 * override toString 
	 * pageID: pageID
	 * namespaceID: namespaceID 
	 * title: title 
	 * categories: categories
	 */
	@Override
	public String toString() {
		return "\npageID: " + this.pageID + ",\nnamespaceID: " + this.namespaceID + ",\ntitle: " + this.title + ",\ncategories: " + this.categories + "\n";
	}

}
