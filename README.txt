	
Wie wird die Berechnung ausgef�hrt ?
	
1) Aufruf der Konsole
2) Den Pfad der Wikixtractor.jar Datei finden
	- Sie befindet sich unter ./dist/
3) Den Pfad in der Konsole �ffnen
4) Option 1 : den Wikixtractor mit dem Befehl :
                inputdatei in ./dist/data mit name pages.txt ablegen
				"java -jar Wikixtractor<nummernfolge>.jar" starten
				Im Men� weiternavigieren ....
   Option 2 : den Wikixtractor mit Inputpfaddatei und Outputpfaddatei Parametern aufrufen :
				"java -jar Wikixtractor<nummernfolge>.jar <path>/input.txt  <path>/output.xml"


	Wo liegt die Ergebnisdatei ?
1)	Option 1 : Die Ergebnisdatei wird bei Erfolg im Dateipfad ./dist/output , mit dem name output.xml vorliegen.
	Option 2 : Die Ergebnisdatei liegt in dem im Befehl angegebenen Outputpfad.
	
	Eine gezipte Ergebnisdatei liegt im Hauptverzeichnis mit 50560 Eintr�gen als "processed outputfile.zip" vor

	Wer hat woran gearbeitet ?
	
	Documentation/Logging - Dominik
	Pages - Ines
	Linkextractor - Andreas Berger
	PageExport - Sebastian
	Pagefactory - Andreas Berger
	Wikixtractor - Andreas Berger
	

	Welche Probleme sind bekannt und bestehen noch ?
1)	Nichtmehr vorhanden : Pfadtrennzeichen / , \
		Probleme beim einlesen des Input/Outputpfades
		
2) leere Kategorien f�r die Page "!Hauptkategorie". 
aktuell noch unproblematisch, jedoch sollte bei zuk�nftiger Verarbeitung der Xml darauf geachtet werden,
dass es auch Pages ohne Kategorien geben kann.