						M E I L E N S T E I N 3



	Wie wird die Berechnung ausgefuehrt ?
	
	-	Den Pfad der .jar-Datei finden 
	-	Sie befindet sich unter EpicPages2.0
	- 	Ihr Name ist "WikiXtractor-0.0.2.jar"
	-	Den Pfad in der Konsole oeffnen
	-	Fuehre die jar-Datei mit folgenden Befehlen aus
			
			erzeuge fertige db:
				(1)-java -jar WikiXtractor-0.0.2.jar createdb <ENTER-DB-Directory> <Enter-Input-Directory>
					startet nacheinander reset, importhtml, categorylinks, articlelinks, entitylinks
			benötigt fertige db:
				(2.i)-java -jar WikiXtractor-0.0.2.jar exportxml <Enter-DB-Directory> <Enter-Output-Directory>
					exportiert vollständige Datenbank zu einer XMl datei im Angegebenen Pfad
				(2.ii)-java -jar WikiXtractor-0.0.2.jar runserver <Enter-DB-Directory>
					startet das webinterface
						suche Page nach titel http://localhost:40407/page?title=Pagetitle
						suche Page/Entity nach id http://localhost:40407/page?id=Pageid
						suche Person nach Vorname http://localhost:40407/person?name=Personname
						suche Person nach Nachname http://localhost:40407/person?surname=Personsurname
						suche City nach Name http://localhost:40407/city?name=Cityname
						suche Monument nach Name http://localhost:40407/monument?name=Monumentname
					
			zusätzliche nützliche Parameter:
				reset <ENTER-DB-Directory>
				importhtml <ENTER-DB-Directory> <Enter-Input-Directory>
				categorylinks <ENTER-DB-Directory>
				articlelinks <ENTER-DB-Directory>
				entitylinks <ENTER-DB-Directory>



	Wo liegt die Ergebnisdatei ?
	
	-	der xml export wird sich im angegebenen Pfad befinden
	-	html ausgaben werden natürlich über localhost:40407/<request> abgefragt



	Wer hat woran gearbeitet ?
	M1-M3
	-	Maven-jar-Builder - Dominik Albert
	-	Pages - Ines, Sebastian Witt
	-	Linkextractor - Andreas Berger
	-	Pagefactory - Andreas Berger
	-	Wikixtractor - Andreas Berger, Sebastian Witt
	- 	Query - Sebastian Witt, Andreas Berger
	-	TaskScheduler - Dominik Albert
	- 	alle Entities - Sebastian Witt, Andreas Berger
	-	Countries - Andreas Berger
	-	Dataminer - Andreas Berger
	
	M4
	-	Database - Andreas Berger
	-	Webinterface - Andreas Berger
	- 	NodeContainer - Andreas Berger
	-	PageExport - Sebastian Witt
 
	

	Welche Probleme sind bekannt und bestehen noch ?
		- 